<?php

namespace nitm\api\actions\view;

use raaly\models\User;
use nitm\api\actions\View;

/**
 * Action is the base class for action classes that implement RESTful API.
 *
 * Need to override ViewAction for User model
 */
class ProfileAction extends View
{
    use \nitm\api\traits\UserQuery, \nitm\api\traits\ProfileAction;

    public $cachable = false;

    public function findModel($id, $options = [], $extraFields = true)
    {
        return parent::findModel($id, array_merge([
            'queryOptions' => [
                'with' => ['user'],
            ],
        ], $options), $extraFields);
    }
}
