<?php

namespace nitm\api\actions\view;

use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecordInterface;
use yii\web\NotFoundHttpException;
use nitm\api\actions\View;

/**
 * Action is the base class for action classes that implement RESTful API.
 *
 * Need to override ViewAction for User model
 *
 */
class FriendAction extends View
{
    use \nitm\api\traits\UserQuery, \nitm\api\traits\FriendAction;

    public function findModel($id)
    {
        return parent::findModel($id, [
            'queryOptions' => [
                'with' => ['user', 'friend']
            ]
        ]);
    }
}
