<?php

namespace nitm\api\actions\view;

use nitm\api\actions\View as BaseAction;

class ConfigAction extends BaseAction 
{
    public $modelClass = '\nitm\api\models\PageConfig';

    /**
     * @inheritdoc
     *
     * @param string | int $id
     * @param array $options
     * @param boolean $extraFields
     * @return void
     */
    public function findModel($id, $options = [], $extraFields = true)
    {
        $modelClass = $this->modelClass;
        return $modelClass::get($id);
    }
}
