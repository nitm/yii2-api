<?php

namespace nitm\api\actions\view;

use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecordInterface;
use yii\web\NotFoundHttpException;
use nitm\api\actions\View;

/**
 * Action is the base class for action classes that implement RESTful API.
 *
 * Need to override Action for User model
 *
 */
class UserAction extends View
{
    use \nitm\api\traits\UserQuery;

    public function findModel($id, $options=[], $extraFields=false)
    {
        return parent::findModel($id, $options, $extraFields);
    }

    public function getParams($modelClass, $id)
    {
        return $this->getParamsForUserQuery($modelClass, $id);
    }
}
