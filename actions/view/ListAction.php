<?php

namespace nitm\api\actions\view;

use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecordInterface;
use yii\web\NotFoundHttpException;
use nitm\api\actions\View;

/**
 * Action is the base class for action classes that implement RESTful API.
 *
 * Need to override ViewAction for Content model
 *
 */
class ListAction extends View
{
    public function getParams($modelClass, $id)
    {
        if (is_numeric($id)) {
            $ret_val['id'] = $id;
        } else {
            $ret_val['slug'] = $id;
        }
        return $ret_val;
    }
}
