<?php

namespace nitm\api\actions;

use Yii;

/**
 * Action is the base class for action classes that implement RESTful API.
 *
 * Need to override UpdateAction for User model
 */
class Update extends \yii\rest\UpdateAction
{
    use \nitm\api\traits\Actions;

    public function run($id)
    {
        if (\Yii::$app->user->isGuest) {
            throw new \yii\web\ForbiddenHttpException();
        }
        $this->beforeRun();
        try {
            return parent::run($id);
        } catch (\Exception $e) {
            if (defined('YII_ENV') && YII_ENV == 'dev') {
                throw $e;
            }
            Yii::warning($e);

            return null;
        }
    }

    protected function beforeRun()
    {
        $model = \Yii::createObject([
            'class' => $this->controller->modelClass,
        ]);
        /*
         * Need to use the raw info in the @var $model->isWhat() index to save the data
         * Here we set request body to ths raw data.
         */
        $body = $this->resolveData($model->isWhat(), $this->resolveData());
        \Yii::$app->getRequest()->setBodyParams($body);

        $this->controller->checkAccess($this->controller->action->id, $model);

        return $model;
    }

    public function checkAccess()
    {
        echo __CLASS__.' Checking access';
        exit;
    }
}
