<?php

namespace nitm\api\actions\delete;

use nitm\api\actions\Delete;

/**
 * Action is the base class for action classes that implement RESTful API.
 *
 * Need to override CreateAction for User model
 */
class FavoriteAction extends Delete
{
    /**
     * Deletes a model.
     * @param mixed $id id of the model to be deleted.
     * @throws ServerErrorHttpException on failure.
     */
    public function run($id)
    {
        $type = $this->resolveData('type');
        if ($type) {
            $model = $this->controller->findModel([
                'item_type' => $type,
                'item_id' => $id,
                'user_id' => \Yii::$app->user->getId()
            ]);
        } else {
            $model = $this->findModel($id);
        }
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }
        $model->delete();
        if (!$model->isDeleted) {
            throw new \yii\web\ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }
    }
}
