<?php

namespace nitm\api\actions\index;

use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecordInterface;
use yii\web\NotFoundHttpException;
use raaly\models\Category;
use nitm\api\models\Friend;
use nitm\api\actions\Index;
use nitm\helpers\ArrayHelper;

/**
 * Action is the base class for action classes that implement RESTful API.
 *
 * Need to override IndexAction for Content model
 *
 */
class FriendAction extends Index
{
    /**
     * Lists all $this->modelClass models.
     * @return mixed
     */
    public function prepareDataProvider($modelClass=null, $options=[], $searchOptions=[])
    {
        $params = \Yii::$app->request->get();
        $params['id'] = ArrayHelper::getValue($params, 'id', 'me');
        $friends = [
            "friends-list" => [
                "id" => $params['id'],
                'pending' => [],
                'mutual' => [],
                'meta' => [
                    'invalidUser' => true
                ]
            ]
        ];

        if ((isset($params['id']) && $params['id'] == 'me') && \Yii::$app->getUser()->isGuest) {
            return $friends;
        }

        $params['id'] = ArrayHelper::getValue($params, 'id', 'me');
        $originalId = $params['id'];
        $friends['friends-list']['id'] = $originalId;
        $user = \Yii::$app->getUser()->getIdentity();
        $networks = [];

        if (!$user->hasRetrievedAllNetworkFriends()) {
            list($networks, $result) = \nitm\api\helpers\FriendHelper::getFromNetworks($user);
            $friends['friends-list']['pending'] += $result;
        } else {
            $clients = array_keys(\Yii::$app->get('authClientCollection')->clients);
            $networks = array_diff_key(array_combine($clients, array_fill(0, count($clients), true)), $user->accounts);
        }

        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $user->getFriends()
        ]);

        foreach ($dataProvider->getModels() as $model) {
            //Support for deleted friends?
            /*if($model->deleted)
                $key = $deleted;
            else*/
            $key = $model->isMutual ? 'mutual' : "pending";
            $model = $model->toArray(array_keys($model->fields()), array_keys($model->extraFields()));
            $model['friends_id'] = $originalId;
            $friends['friends-list'][$key][] = $model;
        }
        $friends['friends-list']['meta'] = [
            "shouldRetrieveFromNetworks" => !$user->hasRetrievedAllNetworkFriends(),
            "signedUpManually" => count($user->accounts) == 0,
            "networks" => $networks
        ];

        return new \yii\data\ArrayDataProvider([
            'allModels' => $friends
        ]);
    }
}
