<?php

namespace nitm\api\actions\index;

use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecordInterface;
use yii\web\NotFoundHttpException;
use nitm\api\models\Favorite;
use nitm\helpers\ArrayHelper;
use nitm\api\actions\Index;

/**
 * Action is the base class for action classes that implement RESTful API.
 *
 * Need to override IndexAction for Content model
 *
 */
class FavoriteAction extends Index
{
    /**
     * Lists all $this->modelClass models.
     * @return mixed
     */
    public function prepareDataProvider($modelClass=null, $options=[], $searchOptions=[])
    {
        $params = \Yii::$app->request->get();

        if ((!isset($params['id']) || isset($params['id']) && $params['id'] == 'me') && \Yii::$app->getUser()->isGuest) {
            return [];
        }

        $params['id'] = ArrayHelper::getValue($params, 'id', 'me');
        $originalId = $params['id'];
        if ($params['id'] == 'me') {
            $params['id'] = \Yii::$app->getUser()->getIdentity()->getId();
        }
        if (is_numeric($params['id'])) {
            $where = ['id' => $params['id']];
        } else {
            $where = ['username' => $params['id']];
        }

        $user = \Yii::$app->user->identity ?: \nitm\api\models\User::find()
            ->select('id')
            ->where($where)->one();
        if (!empty($user)) {
            $searchOptions['user_id'] = $user['id'];
        }
        $dataProvider = parent::prepareDataProvider($this->controller->modelClass, $options, $searchOptions);
        $favorites = [];
        foreach ($dataProvider->getModels() as $model) {
            if ($model->getItemType() == 'undefined') {
                $model->delete();
                continue;
            };
            $favorites[] = $model;
        }
        return new \yii\data\ArrayDataProvider([
            'allModels' => $favorites
        ]);
    }
}
