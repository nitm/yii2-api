<?php

namespace nitm\api\actions\index;

use nitm\api\actions\Index as BaseAction;

class FeedAction extends BaseAction
{
    public $modelClass = '\nitm\api\models\Activity';

    /**
     * Lists all \nitm\api\models\Activity models
     * @return DataProvider
     */
    public function prepareDataProvider($modelClass=null, $options=[], $searchOptions=[], $cache=true)
    {
        $searchOptions = array_merge($searchOptions, [
            'queryOptions' => [
               'with' => [],
               'select' => ['*']
           ],
           'filter' => [
               'verb' => ['create', 'join']
           ]
        ]);
        return parent::prepareDataProvider($modelClass, $options, $searchOptions, $cache);
    }
}
