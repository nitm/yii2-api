<?php

namespace nitm\api\actions\index;

use nitm\api\actions\Index as BaseAction;

class ConfigAction extends BaseAction 
{
    public $modelClass = '\nitm\api\models\PageConfig';

    /**
     * Lists all \nitm\api\models\PageConfig models
     * @return DataProvider
     */
    public function prepareDataProvider($modelClass=null, $options=[], $searchOptions=[], $cache=true)
    {
        $searchOptions = array_merge($searchOptions, [
            //'queryOptions' => [
            //    'with' => [],
            //    'select' => ['*']
            //]
        ]);
        return parent::prepareDataProvider($modelClass, $options, $searchOptions, $cache);
    }
}
