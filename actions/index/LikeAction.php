<?php

namespace nitm\api\actions\index;

use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecordInterface;
use yii\web\NotFoundHttpException;
use raaly\models\Category;
use nitm\api\models\Like;
use nitm\api\actions\Index;
use nitm\helpers\ArrayHelper;

/**
 * Action is the base class for action classes that implement RESTful API.
 *
 * Need to override IndexAction for Content model
 *
 */
class LikeAction extends Index
{
    /**
     * Lists all $this->modelClass models.
     * @return mixed
     */
    public function prepareDataProvider($modelClass=null, $options=[], $searchOptions=[])
    {
        $params = \Yii::$app->request->get();

        if ((!isset($params['id']) || isset($params['id']) && $params['id'] == 'me') && \Yii::$app->getUser()->isGuest) {
            return [];
        }

        $options = [
            'queryOptions' => [
                'with' => ['remote'],
            ]
        ];
        $searchOptions = [];
        $params['id'] = ArrayHelper::getValue($params, 'id', 'me');
        $originalId = $params['id'];
        if ($params['id'] == 'me') {
            $params['id'] = \Yii::$app->getUser()->getIdentity()->getId();
        }
        if (is_numeric($params['id'])) {
            $where = ['id' => $params['id']];
        } else {
            $where = ['username' => $params['id']];
        }

        $user = \nitm\api\models\User::find()
            ->select('id')
            ->where($where)->asArray()->one();
        if (!empty($user)) {
            $searchOptions['user_id'] = $user['id'];
        }
        $dataProvider = parent::prepareDataProvider($this->controller->modelClass, $options, $searchOptions);
        $likes['likes-list']['id'] = $originalId;
        foreach ($dataProvider->getModels() as $model) {
            if ($model->remote_type == 'undefined') {
                $model->delete();
                continue;
            }
            $model = ArrayHelper::toArray($model);
            $model['likes_list_id'] = $originalId;
            $likes['likes-list'][\Yii\helpers\Inflector::pluralize($model['remoteType'])][] = $model;
        }
        return new \yii\data\ArrayDataProvider([
            'allModels' => $likes
        ]);
    }
}
