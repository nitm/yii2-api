<?php

namespace nitm\api\actions\create;

use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecordInterface;
use yii\web\NotFoundHttpException;
use nitm\api\actions\Create;
use nitm\helpers\ArrayHelper;

/**
 * Action is the base class for action classes that implement RESTful API.
 *
 * Need to override CreateAction for Content model
 *
 */
class FavoriteAction extends Create
{
    public $modelClass = 'nitm\api\models\Favorite';
    public $namespace = 'wukdo\rest\models';

    /**
     * Like content. If content is already favorited nothing will be done
     * @param string|int $id
     */
    public function run($modelClass=null, $params=[])
    {
        if (\Yii::$app->getUser()->isGuest) {
            return false;
        }

        $modelClass = $modelClass ?: $this->modelClass;
        extract(\Yii::$app->request->get());

        $type = $this->resolveData('type', null);
        $id = $this->resolveData('id', null);
        $favorite = parent::run($modelClass, [
            'item_type' => $type,
            'item_id' => $id,
            'item_class' => $this->resolveModelClass($type),
            'user_id' => \Yii::$app->user->identity->id
        ]);

        return [
            $favorite->isWhat() => [
                //Have to return an id of 0 here if the model was just created. This is for ember
                'id' => $favorite->item_type.$id,
                'is-favorited' => (boolean)!$favorite->isDeleted,
            ]
        ];
    }


    protected function resolveModelClass($name)
    {
        $namespace = $this->namespace ?: __NAMESPACE__;
        $class = $namespace.'\\Related'.\nitm\helpers\ClassHelper::properFormName($name);
        if (class_exists($class)) {
            return $class;
        } else {
            $class = $namespace.'\\'.\nitm\helpers\ClassHelper::properFormName($name);
            if (class_exists($class)) {
                return $class;
            }
        }
        return null;
    }
}
