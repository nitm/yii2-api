<?php

namespace nitm\api\actions\create;

use nitm\api\actions\Create;
use nitm\api\models\Like;

/**
 * Action is the base class for action classes that implement RESTful API.
 *
 * Need to override CreateAction for Content model
 */
class LikeAction extends Create
{
    public $modelClass = 'nitm\api\models\Like';

    /**
     * Like content. If content is already favorited nothing will be done.
     *
     * @param string|int $id
     */
    public function run($modelClass = null, $type = null, $id = null)
    {
        if (\Yii::$app->getUser()->isGuest) {
            return false;
        }

        extract($this->resolveData());

        list($like, $params) = parent::run(Like::className(), $type, $id);

        //Is there already a like? If so update its value
        if ($like === null) {
            $like = new Like(array_merge([
                'scenario' => 'create',
                'value' => $this->getLikeValue(),
            ], (array) $params));
        } else {
            $like->setScenario('update');
            if (in_array($this->controller->id, ['like', 'likes'])) {
                $like->value = ($like->value == 1 ? 0 : 1);
            } else {
                $like->value = ($like->value == -1 ? 0 : -1);
            }
        }
        $like->type = $this->controller->id;
        $like->save();

        return [
            $like->isWhat() => [
                //Have to return an id of 0 here if the model was just created. This is for ember
                'id' => $params['remote_type'].$id,
                'is-liked' => $like->value === 1,
                'is-disliked' => $like->value === -1,
                'result' => 'test',
            ],
        ];
    }

    protected function getLikeValue()
    {
        $values = [
            'dislikes' => -1,
            'likes' => 1,
            'dislike' => -1,
            'like' => 1,
        ];

        return $values[$this->controller->id];
    }
}
