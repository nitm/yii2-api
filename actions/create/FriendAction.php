<?php

namespace nitm\api\actions\create;

use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecordInterface;
use yii\web\NotFoundHttpException;
use nitm\models\Category;
use nitm\api\models\Friend;
use nitm\api\models\User;
use nitm\api\actions\Create;
use nitm\helpers\ArrayHelper;

/**
 * Action is the base class for action classes that implement RESTful API.
 *
 * Need to override CreateAction for Content model
 *
 */
class FriendAction extends Create
{
    /**
     * Like content. If content is already friendd nothing will be done
     * @param string|int $id
     */
    public function run($modelClass=null, $type=null, $id=null)
    {
        if (\Yii::$app->getUser()->isGuest) {
            return false;
        }

        extract($this->resolveData('friend', []));

        /**
         * @var $friendId Should match the friend who this user wants to connect to
         * @var $userId Should match the id of the logged in user
         * @var [type]
         */
        if (!\Yii::$app->user->isGuest && isset($friendId) && isset($userId) && (
            \Yii::$app->user->identity->getId() == $userId ||
            \Yii::$app->user->identity->username == $userId
        ) && ($friend = \Yii::$app->user->identity->addFriend($friendId)) !== null) {
            return [
                $friend->isWhat() => $friend->toArray([], [])
            ];
        }
        return [
            'errors' => [
                'user' => ["Could't validate friendship"]
            ]
        ];
    }

    protected function getMock()
    {
        $friend = new Friend([
           'id' => $userId,
       ]);
        $friend->populateRelation('user', \Yii::$app->user->identity);
        $friend->populateRelation('friend', \Yii::$app->user->identity);
        return [
           $friend->isWhat() => $friend->toArray([], array_keys($friend->extraFields()))
       ];
    }
}
