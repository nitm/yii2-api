<?php

namespace nitm\api\actions\update;

use nitm\filemanager\helpers\ImageHelper;
use nitm\filemanager\helpers\UploadHelper;
use nitm\filemanager\models\Image;
use nitm\api\traits\ProfileAction as ProfileActionTrait;
use nitm\api\actions\Update as BaseUpdateAction;

/**
 * Action is the base class for action classes that implement RESTful API.
 *``
 * Need to override UpdateAction for Content model.
 */
class ProfileAction extends BaseUpdateAction
{
    use \nitm\api\traits\UserQuery, ProfileActionTrait;

    public function run($id = null)
    {
        $model = parent::run($id ?: \Yii::$app->user->id);
        // $stream = null;
        // if (UploadHelper::hasUploadedData(true, $stream)) {
        //     if (in_array(strtolower(\Yii::$app->getRequest()->method), ['put', 'patch'])) {
        //         $method = 'saveFromStdIn';
        //     } else {
        //         $method = 'saveImages';
        //     }

        //     $image = null;
        //     if (($images = ImageHelper::$method(new Image([
        //         'remote_type' => 'profile',
        //         'remote_id' => $model->getId()
        //     ]), 'profile', $model->getId(), 'file', $stream)) !== null) {
        //         $image = current($images);
        //     }
        //     if ($image) {
        //         $model->background_image = $image->url();
        //         $model->save();
        //     }
        // }
        return $model;
    }
}
