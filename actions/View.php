<?php

namespace nitm\api\actions;

use yii\db\ActiveRecordInterface;
use yii\web\NotFoundHttpException;
use nitm\helpers\Cache;

/**
 * Action is the base class for action classes that implement RESTful API.
 *
 * Need to override ViewAction for User model
 */
class View extends \yii\rest\ViewAction
{
    use \nitm\api\traits\Actions;

    /**
     * Returns the data model based on the primary key given.
     * If the data model is not found, a 404 HTTP exception will be raised.
     *
     * @param string $id the ID of the model to be loaded. If the model has a composite primary key,
     *                   the ID must be a string of the primary key values separated by commas.
     *                   The order of the primary key values should follow that returned by the `primaryKey()` method
     *                   of the model
     *
     * @return ActiveRecordInterface the model found
     *
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id, $options = [], $extraFields = true)
    {
        $modelClass = $this->modelClass;
        if ($this->cachable) {
            return Cache::remember([$modelClass::isWhat(), $id, \Yii::$app->user->identity ? \Yii::$app->user->identity->apiToken->token : null], function () use ($modelClass, $id, $options, $extraFields) {
                return $this->localFind($id, $options, $extraFields, $modelClass);
            });
        } else {
            return $this->localFind($id, $options, $extraFields, $modelClass);
        }
    }

    private function localFind($id, $options, $extraFields, $modelClass = null)
    {
        $modelClass = $modelClass ?: $this->modelClass;
        //Doing this here becuase Document DB sometimes returns errors that are recoverable
        $needToFind = true;
        while ($needToFind) {
            try {
                $model = $this->findModelInternal($id, $options, $extraFields);
                $needToFind = false;
            } catch (\Exception $e) {
                if (!in_array(get_class($e), [
                  '\MongoDB\Driver\Exception\SSLConnectionException',
             ])) {
                    $needToFind = false;
                    $model = null;
                }
            }
        }

        if ($model) {
            //Need to serialize according to controller serializer
           return $model;
        } else {
            throw new \yii\web\NotFoundHttpException($modelClass::isWhat()." not found: $id");
        }
    }
}
