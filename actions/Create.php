<?php

namespace nitm\api\actions;

/**
 * Action is the base class for action classes that implement RESTful API.
 *
 * Need to override CreateAction for User model
 */
class Create extends \yii\rest\CreateAction
{
    use \nitm\api\traits\Actions;

    /**
     * Ember requires extra support
     * @var boolean
     */
    public $enableEmberSupport;

    public function run($modelClass = null, $params=[])
    {
        if (\Yii::$app->user->isGuest) {
            throw new \yii\web\ForbiddenHttpException();
        }

        $modelClass = $modelClass ?: $this->modelClass ?: $this->controller->modelClass;
        $params = empty($params) ? $this->resolveData() : $params;

        if ($this->enableEmberSupport) {
            extract($this->controller->extractEmberPayload($params, array_keys($params)));
        }

        $model = new $modelClass([
            'scenario' => 'create'
        ]);

        if($model->load($params, '')) {
          $this->controller->checkAccess($this->controller->action->id, $model);
          if ($model->save()) {
              return $model;
          } else {
              throw new \yii\web\BadRequestHttpException(\nitm\helpers\Model::formatErrors($model));
          }
        } else {
          throw new \yii\web\BadRequestHttpException(\nitm\helpers\Model::formatErrors($model));
        }
    }
}
