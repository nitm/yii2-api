<?php

namespace nitm\api\actions;

/**
 * Action is the base class for action classes that implement RESTful API.
 *
 * Need to override IndexAction for User model
 */
class Index extends \yii\rest\IndexAction
{
    use \nitm\api\traits\Actions;

    /**
     * Lists all $this->modelClass models.
     *
     * @return mixed
     */
    public function prepareDataProvider($modelClass = null, $options = [], $searchOptions = [])
    {
        $modelClass = $modelClass ?: $this->controller->modelClass;
        $model = new $modelClass();
        $model->beginSearch($options);

        return $model->search($searchOptions);
    }
}
