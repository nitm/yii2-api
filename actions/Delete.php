<?php

namespace nitm\api\actions;

/**
 * Action is the base class for action classes that implement RESTful API.
 *
 * Need to override CreateAction for User model
 */
class Delete extends \yii\rest\DeleteAction
{
    use \nitm\api\traits\Actions;
}
