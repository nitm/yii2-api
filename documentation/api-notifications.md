# Get notifications

Get the notifications. If no type is specified return notifications independent of other parameters.

## GET <http://{{apiUrl}}/notifications>

### Access Level:

Current User

### Limit

20 items per call

### Params

None

### Return value

A json array of notifications

Array members in _italics_ are optional

```
[
    {
        id: The numeric id of the notification,
        user: The user who performed the activity,
        object: The remove object that this notification belongs to,
        action: The action that was performed,
    }
    ...
]
```
