# Activity

Get home page activity. As described: (<https://github.com/activitystreams/activity-schema/blob/master/activity-schema.md>) and (<http://activitystrea.ms/specs/json/1.0/>)

## GET <http://{{apiUrl}}/activity>

### Access Level:

Public | User preferences

### Limit

20 items per call

### Params

None

### Return value

A json array of activity

Array members in _italics_ are optional

```
[
    {
        id: The numeric id of the activity,
        published: The timestamp of publication,
        displayName: The display name for this activity,
        actor: {
          objectType: 'user|system|job',
          displayName: The display name for this actor,
          icon: {
            url: URL
          }
        },
        object: {
          objectType: job,
          displayName: The display name for this object,
          ...
        },
        target: {
          objectType: jobList,
          displayName: The display name for this object,
          ...
        },
        verb: The action that was taken,
    }
    ...
]
```
