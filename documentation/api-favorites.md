# Favorites

The favorites endpoint supports creating, listing a single favorite, listing all favorites and updating favorites

## Get All Favorites: GET <https://{{apiUrl}}/favorite>

Get all favorites. If no type is specified return favorites independent of other parameters.

### Access Level:

Logged in User

### Limit

20 items per call

### Return value

A json array of favorites

Array members in _italics_ are optional

```
{
    success: true,
    data: [
      {
        id: The numeric id of the favorite,
        itemType: The type of the favorited item,
        itemId: The id of the favorited item,
        item:  {
            id: The item's id,
            ...
        }
      },
      ...
    ],
}
```

## Get Favorite Information: GET <https://{{apiUrl}}/favorite/{{id}}|{{type}}/{{id}}>

Get a specific favorite information.

### Access Level:

Logged In User

### Limit

1 item per call

### Params

These parameters can be specified using pretty URL syntax (/value0/value1) or using GET parameters (param=value)

```
{{type: Optional}}: The type of the content that is favorited
{{id}}: Get a favorite based on the specified ID. This is either the items id, in conjunction with `{{type}}` or the favorite ID by itself
```

### Return value

#### On Success 200 OK

Array members in _italics_ are optional

```
{
  success: true,
  data: {
    id: The numeric id of the favorite,
    itemType: The type of the favorited item,
    itemId: The id of the favorited item,
    item:  {
        id: The item's id,
        ...
    }
  },
}
```

#### On Failure 404 Not Found | 400 Bad Request

```
{
    success: false,
    error: Error message
}
```

## Create Favorite: POST <https://{{apiUrl}}/favorite/{{type}}/{{id}}>

Creates a new favorite. The data can be sent as URL parameters or as the body of the request

### Access Level

Logged in User

### Limit

### Params

These parameters can be specified using pretty URL syntax (/value0/value1) or using GET parameters (param=value)

```
{{type: Optional}}: The type of the content
{{id}}: Get a favorite based on the specified ID. This is either the items id, in conjunction with `{{type}}` or the favorite ID by itself
```

### Body

This is the optional data that can be specified for a favorite

```
{
    type: The type of the data,
    id: The id of the data
}
```

### Return value

The newly created favorite or an error

Array members in _italics_ are optional

#### On Success

```
{
  success: true,
  data: {
    id: The numeric id of the favorite,
    itemType: The type of the favorited item,
    itemId: The id of the favorited item,
    item:  {
        id: The item's id,
        ...
    }
  },
}
```

#### On Failure 404 Not Found | 400 Bad Request

```
{
  success: false,
  error: 'Couldn't create favorite'
}
```

## Delete Favorite: DELETE <https://{{apiUrl}}/favorite/{{id?}}|{{type}}/{{id}}>

Deletes a favorite.

### Access Level

Favorite | Admin.

### Limit

### Params

These parameters can be specified using pretty URL syntax (/value0/value1) or using GET parameters (param=value)

```
{{: Optional}}: The type of the favorite to be deleted
{{id}}: Get a favorite based on the specified ID. This is either the items id, in conjunction with `{{type}}` or the favorite ID by itself
```

### Return value

The deleted favorite or an error

#### On Success

```
{
  success: true,
  data: {
    id: The id of the favorite,
  },
}
```

#### On Failure 404 Not Found | 400 Bad Request

```
{
  success: false,
  error: 'Couldn't find the specified favorite'
}
```
