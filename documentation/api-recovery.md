# Handle user recovery using this API endpoint

Use this API endpoint to authenticate Wukdo users to the backend.

## Url

`POST https://{{apiUrl}}/recovery/{action}`

Where action can be:

```
request: Request password recovery
reset: Reset the password of the user
```

## Request Reset `POST https://{{apiUrl}}/recovery/request`

### Access Level:

Public

### Params

```
{
  email: Email address,
  password: The password of the user
}
```

### Return value

Either a json array containing the request result or an array containing error messages

Array members in _italics_ are optional

#### On Success

```
{
    success: true,
    forgotRequest: {
      message: @string
    }
}
```

#### On Failure

```
{
  success: false,
  data: {
    error: {
      message: @string
    }
  }
}
```

## Perform Reset `POST https://{{apiUrl}}/recovery/reset/{id}/{code}`

### Access Level:

Public

### Params

```
{
  id: The user id we're confirming,
  code: The connect code for this social account
}
```

### Return value

Either a json array containing the reset result or error messages

Array members in _italics_ are optional

#### On Success

```

{
  success: true,
  data: {
    confirmUser: {
      isConfirmed: true|false
    }
  }
}
```

#### On Failure

```
{
  success: false,
  data: {
    error: {
      message: @string
    }
  }
}
```
