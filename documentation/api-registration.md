# Handle user registration using this API endpoint

Use this API endpoint to authenticate Wukdo users to the backend.

## Url

`POST https://{{apiUrl}}/registration/{action}`

Where action can be:

```
register: Register a new user
connect: Connect a social account
confirm: Confirm an account
resend: Resend the verification email
```

## Register a New User `POST https://{{apiUrl}}/registration/register`

### Access Level:

Public

### Params

These parameters are required when registering in a user

```
{
  email: A string containing the user's mail,
  username: A string containing the user's mail,
  password: A string containing the user's password
}
```

### Return value

Either a json array containing the logged in user's credentials or an array containing error messages

Array members in _italics_ are optional

#### On Success

```
{
    success: true,
    data: {
      user: {
        id: The user id,
        username: The user's username,
        email: The user's email address
      },
      register: {
        success: true|false
      }
    }
}
```

#### On Error

```
{
    success: false,
    error: @string
}
```

## Connect a Social Account `POST https://{{apiUrl}}/registration/connect/{code}`

### Access Level:

Public

### Params

```
{
  code: The connect code for this social account
}
```

### Return value

Either a json array containing the logged in user's credentials or an array containing error messages

Array members in _italics_ are optional

#### On Success

```
{
    success: true,
    data: {
      user: {
        id: The user id,
        username: The user's username,
        email: The user's email address
      },
      connect: {
        success: true|false
      }
    }
}
```

### To connect a social account the following steps are required:

 1. Send a `GET` request to `https://{{apiUrl}}/security/auth?authclient={{facebook|google}}`
 2. Make sure that the frontend has a route/page to handle the `https://{{registerUrl}}/{{code}}`
   - You may set the `{{registerUrl}}` by appending the `r` param `r={{registerUrl}}` to the auth request otherwise the API will return the user to `https://{{webUrl}}/registration/connect/{{code}}` by default
 3. Send the code to the API as a `POST` request to `https://{{apiUrl}}/registration/connect/{{code}}`
 4. You will received the logged in user credentials once the user has been logged in or an error message if there is an error

## Confirm a New User `POST https://{{apiUrl}}/registration/confirm/{id}/{code}`

### Access Level:

Public

### Params

```
{
  id: The user id we're confirming,
  code: The connect code for this social account
}
```

### Return value

Either a json array containing the confirmation result or error messages

Array members in _italics_ are optional

#### If user is logged in

```

{
  success: true,
  data: {
    confirmUser: {
      isConfirmed: true|false
    }
  }
}
```

#### If user is not logged in

```
{
  success: false,
  data: {}
}
```

## Resend Confirmation Email `POST https://{{apiUrl}}/registration/resend`

### Access Level:

Public

### Params

```
{
  email: The email address of the account we're confirming
}
```

### Return value

Either a json array containing the resend result or error messages

Array members in _italics_ are optional

#### If user is logged in

```

{
  success: true,
  data: {
    resendConfirm: {
      message: @string
    }
  }
}
```

#### If user is not logged in

```
{
  success: false,
  data: @array
}
```
