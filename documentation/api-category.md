# Categories

The categories endpoint supports creating, listing a single category, listing all categories and updating categories

## Get All Categories: GET <http://{{apiUrl}}/category>

Get all categories. If no type is specified return categories independent of other parameters.

### Access Level:

Public

### Limit

20 items per call

### Params

These parameters can be specified using pretty URL syntax (/value0/value1) or using GET parameters (param=value)

```
{
  category: One of CSV [string names | numeric category ids | all]. Default is 'all'. If a category is specified then only sub categories of that category are returned
  return: CSV list of return values. Set to 'all' to return everything. Default returns [id, title, type, description, author, slug, images]. To return the default plus extra specify a value such as: return=default
  sort: One of [date, name] date by default (-{sort} specifies descending order)
}
```

### Return value

A json array of categories

Array members in _italics_ are optional

```
{
  data:  [
    {
        id: The numeric id of the category,
        name: The name of the category
        type: The string type of the category,
        description: The text for the category,
        images: [
            {
                url: Url of the image
                title: The title of the image
            }
            ...
        ],
        author: {
            id: The id of the author
            username: The username of the author
            avatar: The url for the avatar of the author
                                }
        slug: The slug for the category
    },
    ...
  ]
}
```

## Get Category Information: GET <http://{{apiUrl}}/category/{id}>

Get a specific category information.

### Access Level:

Public

### Limit

1 items per call

### Params

These parameters can be specified using pretty URL syntax (/value0/value1) or using GET parameters (param=value)

```
{id}: Get a category based on the specified ID
```

### Return value

#### On Success 200 OK

Array members in _italics_ are optional

```
{
  success: true,
  data: {
        id: The numeric id of the category,
        name: The name of the category
        type: The string type of the category,
        description: The text for the category,
        images: [
            {
                url: Url of the image
                title: The title of the image
            }
            ...
        ],
        author: {
            id: The id of the author
            username: The username of the author
            avatar: The url for the avatar of the author
                }
        slug: The slug for the category
    }
}
```

#### On Failure 404 Not Found | 400 Bad Request

```
{
    success: false,
    error: Error message
}
```

## Create Category: POST <http://{{apiUrl}}/category>

Creates a new category.

### Access Level

`Admin`

### Limit

### Params

These parameters can be specified using pretty URL syntax (/value0/value1) or using GET parameters (param=value)

```
{category}: One of CSV [string names | numeric category ids | all]. Default is 'all'
```

### Return value

The newly created category or an error

Array members in _italics_ are optional

#### On Success

```
{
    success: true,
    data: {
        id: The numeric id of the category,
        name: The name of the category
        type: The string type of the category,
        description: The text for the category,
        images: [
            {
                url: Url of the image
                title: The title of the image
            }
            ...
        ],
        author: {
            id: The id of the author
            username: The username of the author
            avatar: The url for the avatar of the author
                }
        slug: The slug for the category
   },
}
```

#### On Failure 404 Not Found | 400 Bad Request

```
{
  success: false,
  error: Error Message
}
```

## Update Category: POST <http://{{apiUrl}}/category/{id}>

Creates a new category.

### Access Level

`Admin`

### Limit

### Params

These parameters can be specified using pretty URL syntax (/value0/value1) or using GET parameters (param=value)

```
{id}: The ID of the category to be updated
```

### Return value

The newly created category or an error

Array members in _italics_ are optional

#### On Success

```
{
    success: true,
    data: {
        id: The numeric id of the category,
        name: The name of the category
        type: The string type of the category,
        description: The text for the category,
        images: [
            {
                url: Url of the image
                title: The title of the image
            }
            ...
        ],
        author: {
            id: The id of the author
            username: The username of the author
            avatar: The url for the avatar of the author
                }
        slug: The slug for the category
    },
}
```

#### On Failure 404 Not Found | 400 Bad Request

```
{
    success: false,
    error: Error message
}
```

## Delete Category: DELETE <http://{{apiUrl}}/category/{id}>

Deletes a category.

### Access Level

`Admin`.

### Limit

### Params

These parameters can be specified using pretty URL syntax (/value0/value1) or using GET parameters (param=value)

```
{id}: The ID of the category to be deleted
```

### Return value

The deleted category or an error

Array members in _italics_ are optional

#### On Success

```
{
    success: true,
    data: {
        id: The numeric id of the category,
        name: The name of the category
        type: The string type of the category,
        description: The text for the category,
        images: [
            {
                url: Url of the image
                title: The title of the image
            }
            ...
        ],
        author: {
            id: The id of the author
            username: The username of the author
            avatar: The url for the avatar of the author
                }
        slug: The slug for the category
    },
}
```

#### On Failure 404 Not Found | 400 Bad Request

```
{
    success: false,
    error: Error message
}
```
