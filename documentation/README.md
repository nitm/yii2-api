# API

Welcome to the boilerplate API documentation. Please customize this dfocumentation to your needs

## Technology

The NITM API is built using various technologies and frameworks:

- PHP: `7.0`
- Yii2: `>=2.0.5`
- NITM yii2-cms: `stable`
- Postgresql: `9.5`
- Redis: `3.0.6`
- Security: `HTTP/2.0 TLS`

## Environments

The API can be run in either production or development mode.
