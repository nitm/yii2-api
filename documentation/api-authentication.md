# Handle user authentication using this API endpoint

Use this API endpoint to authenticate Wukdo users to the backend.

## Url

`POST https://{{apiUrl}}/security/{{action}}`

Where action can be:

```
login: Log in a user
social: The last step to creating a client account after they have been logged in through the client side APP
logout: Log out a user
ping: Refresh a users authentication by either `COOKIE` or `token`
```

## Authentication methods

Authentication is supported using token authentication.

### Web Application

Authentication is supported by access token propagation. The front end should poll for current session status using the `ping` action.

### Mobile Application

PHP Session authentication and re-authentication using an auth token.

## Token Authentication

On `social` the API will return the following header. Store this token in session storage:

```
Access-Token: {{token}}
```

For subsequent calls to the API the `Authorization` header should be set using HTTP bearer for delivering the token

```
Authorization: Bearer {{token}}
```

The APi with determine the proper authentication access for the user

## Client Side Social Authentication/Login `POST https://{{apiUrl}}/security/social/{{code}}?provider={{provider}}`

### Access Level:

Public

### Params

These parameters are required when logging in a user

```
{
    id: The id returned by the oAuth process on the client side,
    access-token: The access token returned from user side authentication,
    provider: The provider string. One of [facebook, google],
    data: {
        id: The result's unique id for the social network. NOT the user ID,
        ... Any extra data such as email, avatar...etc for the user's account
    }
}
```

### Return value

Either a json array containing the logged in user's credentials or an array containing error messages

Array members in _italics_ are optional

#### On Success

```
{
    success: true,
    data: {
        id: @string The user id,
        username: @string The user's username,
        joined: @date Date the user joined,
        avatar: @url Avatar url,
        fullName: @string The user's full name,
        profile: @string User Id,
        user: @string User Id
    }
}
```

#### On Error

```
{
    success: false,
    error: String containing the error message
}
```

## Login `POST https://{{apiUrl}}/security/login`

### Access Level:

Public

### Params

These parameters are required when logging in a user

```
{
  login: A string containing the user's mail,
  password: A string containing the user's password
}
```

### Return value

Either a json array containing the logged in user's credentials or an array containing error messages

Array members in _italics_ are optional

#### On Success

```
{
    success: true,
    data: {
        id: @string The user id,
        username: @string The user's username,
        joined: @date Date the user joined,
        avatar: @url Avatar url,
        fullName: @string The user's full name,
        profile: @string User Id,
        user: @string User Id
    }
}
```

#### On Error

```
{
    success: false,
    error: String containing the error message
}
```

## Logout `POST https://{{apiUrl}}/security/logout`

### Access Level:

Public

### Params

None

### Return value

Either a json array containing the logged in user's credentials or an array containing error messages

Array members in _italics_ are optional

#### On Success

```
{
    success: true
}
```

## Ping `POST https://{{apiUrl}}/security/ping`

### Access Level:

Public

### Params

None

### Return value

Either a json array containing the logged in user's credentials or an array containing error messages

Array members in _italics_ are optional

#### If user is logged in

```
{
  success: true,
  data: {
    id: The user id,
    username: The user's username,
    email: The user's email address
  }
}
```

#### If user is not logged in

```
{
  success: false,
  data: {}
}
```
