# Users

The users endpoint supports creating, listing a single user, listing all users and updating users

## Get All Users: GET <https://{{apiUrl}}/user>

Get all users. If no type is specified return users independent of other parameters.

### Access Level:

Public

### Limit

20 items per call

### Params

These parameters can be specified using pretty URL syntax (/value0/value1) or using GET parameters (param=value)

```
{
  category: One of CSV [string names | numeric category ids | all]. Default is 'all'
  return: CSV list of return values. Set to 'all' to return everything. Default returns [id, title, type, description, author, slug, images, category, location]. To return the default plus extra specify a value such as: return=default,location
  location: One of CSV [string names | numeric location ids]. Use the location api to find ingredients by location. Default is 'all'
  sort: One of [date, name, category] date by default (-{sort} specifies descending order)
}
```

### Return value

A json array of users

Array members in _italics_ are optional

```
{
    success: true,
    data: [
      {
        id: The numeric id of the user,
        username: The username of the user,
        email: The user's email address,
        type: The string type of the user
      }, {
        id: The numeric id of the user,
        username: The username of the user,
        email: The user's email address,
        type: The string type of the user
      },
      ...
    ],
}
```

## Get User Information: GET <https://{{apiUrl}}/user/{id}>

Get a specific user information.

### Access Level:

Public

### Limit

1 item per call

### Params

These parameters can be specified using pretty URL syntax (/value0/value1) or using GET parameters (param=value)

```
{id}: Get a user based on the specified ID
```

### Return value

#### On Success 200 OK

Array members in _italics_ are optional

```
{
  success: true,
  data: {
    id: The numeric id of the user,
    username: The username of the user,
    email: The user's email address,
    type: The string type of the user
  },
}
```

#### On Failure 404 Not Found | 400 Bad Request

```
{
    success: false,
    error: Error message
}
```

## Create User: POST <https://{{apiUrl}}/user>

Creates a new user.

### Access Level

Logged in User

### Limit

### Params

These parameters can be specified using pretty URL syntax (/value0/value1) or using GET parameters (param=value)

```
{category}: One of CSV [string names | numeric category ids | all]. Default is 'all'
```

### Body

This is the data that can be updated for a specific user

```
{
    username: string,
    password: string,
    email: string The account email,
    avatar: file,
    profile: {
        name: string First and Last name,
        public_email: string The public facing email,
        website: string The users website,
        bio: string (255) A short bio,
        timezone: string Timezone: America/New_York,
        industry_id: integer The industry id the user associates with,
        country_id: integer The user's country id,
        dob: date The date the user was born,
        mobile: integer The user's mobile phone number, including area code: 17771234567
    }
}
```

### Return value

The newly created user or an error

Array members in _italics_ are optional

#### On Success

```
{
  success: true,
  data: {
    id: The numeric id of the user,
    username: The username of the user,
    email: The user's email address,
    type: The string type of the user
  },
}
```

#### On Failure 404 Not Found | 400 Bad Request

```
{
  success: false,
  error: 'Couldn't create user'
}
```

## Update User: POST <https://{{apiUrl}}/user/{id}>

Creates a new user.

### Access Level

Logged in User

### Limit

### Params

These parameters can be specified using pretty URL syntax (/value0/value1) or using GET parameters (param=value)

```
{id}: The ID of the user to be updated
```

### Body

This is the data that can be updated for a specific user

```
{
    username: string,
    password: string,
    email: string The account email,
    avatar: file,
    profile: {
        name: string First and Last name,
        public_email: string The public facing email,
        website: string The users website,
        bio: string (255) A short bio,
        timezone: string Timezone: America/New_York,
        industry_id: integer The industry id the user associates with,
        country_id: integer The user's country id,
        dob: date The date the user was born,
        mobile: integer The user's mobile phone number, including area code: 17771234567
    }
}
```

### Return value

The newly created user or an error

Array members in _italics_ are optional

#### On Success

```
{
  success: true,
  data: {
    id: The numeric id of the user,
    username: The username of the user,
    email: The user's email address,
    type: The string type of the user
  },
}
```

#### On Failure 404 Not Found | 400 Bad Request

```
{
    success: false,
    error: Error Message
}
```

## Delete User: DELETE <https://{{apiUrl}}/user/{id}>

Deletes a user.

### Access Level

User | Admin.

### Limit

### Params

These parameters can be specified using pretty URL syntax (/value0/value1) or using GET parameters (param=value)

```
{id}: The ID of the user to be deleted
```

### Return value

The deleted user or an error

#### On Success

```
{
  success: true,
  data: {
    id: The numeric id of the user,
    username: The username of the user,
    email: The user's email address,
    type: The string type of the user
  },
}
```

#### On Failure 404 Not Found | 400 Bad Request

```
{
  success: false,
  error: 'Couldn't find the specified user'
}
```
