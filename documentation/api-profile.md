# User Profiles

The users endpoint supports listing a single user profile, and updating a user's profile and user information

## Get User Profile Information: GET <https://{{apiUrl}}/profile/{id}>

Get a specific user information.

### Access Level:

Public

### Limit

1 item per call

### Params

These parameters can be specified using pretty URL syntax (/value0/value1) or using GET parameters (param=value)

```
{id}: Optional Get a user based on the specified ID. If no `id` is provided the user is resolved based on the API token
```

### Return value

#### On Success 200 OK

Array members in _italics_ are optional

```
{
  success: true,
  data: {
      profile: {
          id: The username of the user,
          name: The name of the user,
          ...,
          user: {
            id: The username of the user,
            joined: THe date the user joined,
            avatar: The users avatar url
            fullName: The user's fullname,
            role: The user's role
          }
      }
  },
}
```

#### On Failure 404 Not Found | 400 Bad Request

```
{
    success: false,
    error: Error message
}
```

## Update User Profile: POST|PATCH <https://{{apiUrl}}/profile/{id}>

Updates a user profile.

### Access Level

Logged in User Profile

### Limit

### Params

These parameters can be specified using pretty URL syntax (/value0/value1) or using GET parameters (param=value)

```
{id: Optional}: The ID of the user to be updated. If no {id} is provided the user is resolved based on the API token
```

### Body

This is the data that can be updated for a specific profile

```
{

    name: string First and Last name,
    public_email: string The public facing email,
    website: string The users website,
    bio: string (255) A short bio,
    timezone: string Timezone: America/New_York,
    dob: date The date the user was born,
    mobile: integer The user's mobile phone number, including area code: 17771234567,
    user: {
        username: string,
        password: string,
        email: string The account email,
        avatar: file,
    }
}
```

### Return value

The newly created user or an error

Array members in _italics_ are optional

#### On Success

```
{
  success: true,
  data: {
      The changed attributes
  },
}
```

#### On Failure 404 Not Found | 400 Bad Request

```
{
    success: false,
    error: Error Message
}
```
