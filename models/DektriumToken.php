<?php

namespace nitm\api\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "nitm_api_token".
 *
 * @property integer $id
 * @property string $token
 * @property integer $user_id
 *
 * @property User $user
 */
class DektriumToken extends \dektrium\user\models\Token
{
    /**
     * @return string
     */
    public function getUrl()
    {
        switch ($this->type) {
            case self::TYPE_CONFIRMATION:
                $route = '/registration/confirm';
                break;
            case self::TYPE_RECOVERY:
                $route = '/recovery/reset';
                break;
            case self::TYPE_CONFIRM_NEW_EMAIL:
            case self::TYPE_CONFIRM_OLD_EMAIL:
                $route = '/settings/confirm';
                break;
            default:
                throw new \RuntimeException();
        }

        return Url::to([
            $route, 
            'id' => $this->user_id, 
            'code' => $this->code,
            '_format' => 'html',
        ], true);
    }
}
