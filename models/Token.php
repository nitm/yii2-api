<?php

namespace nitm\api\models;

use Yii;

/**
 * This is the model class for table "nitm_api_token".
 *
 * @property integer $id
 * @property string $token
 * @property integer $user_id
 *
 * @property User $user
 */
class Token extends \nitm\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nitm_api_token';
    }

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['dates'] = [
            'class' => \nitm\behaviors\DateAttributes::class,
            'attributes' => ['expires_at']
        ];
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $userClass = \Yii::$app->user->identityClass;
        return [
            [['user_id'], 'required', 'on' => ['create', 'update']],
            [['user_id'], 'integer'],
            [['token'], 'string', 'max' => 255],
            [['user_id'], 'unique'],
            [['token'], 'filter', 'filter' => [$this, 'generate'], 'on' => ['create']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => $userClass, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'token' => Yii::t('app', 'Token'),
            'user_id' => Yii::t('app', 'User ID'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'create' => ['user_id', 'token', 'expires_at'],
            'update' => ['user_id', 'token', 'expires_at'],
            'default' => ['user_id', 'token', 'expires_at'],
            'generate' => ['user_id', 'token', 'expires_at']
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        $userClass = \Yii::$app->user->identityClass;
        return $this->hasOne($userClass, ['id' => 'user_id'])->inverseOf('apiToken');
    }

    public function getIsPermanent()
    {
        return in_array($this->is_permanent, [true, 1]);
    }

    public function getIsExpired()
    {
        if ($this->isPermanent) {
            return false;
        }
        return strtotime($this->expires_at) < strtotime('now');
    }

    public function regenerate()
    {
        $module = \Yii::$app->getModule('nitm-api');
        $this->token = $this->generate();
        $expiresAt = strtotime('now') + ($module ? $module->tokenExpiry : 3600);
        $this->expires_at = $expiresAt;
        $this->save();
    }

    public function generate()
    {
        $module = \Yii::$app->getModule('nitm-api');
        $expiresAt = strtotime('now') + ($module ? $module->tokenExpiry : 3600);
        $this->expires_at = $expiresAt;
        $this->setScenario('generate');
        $this->token = \Yii::$app->security->generateRandomString();
        return $this->token;
    }

    public static function findByKey($token)
    {
        return static::findOne(['token' => $token]);
    }
}
