<?php

namespace nitm\api\models;

/**
  * This class extends dektriums LoginForm model and adds extra functionality after login.
  */
 use dektrium\user\models\LoginForm as BaseLoginForm;

 class LoginForm extends BaseLoginForm
 {
     public function login()
     {
         $ret_val = parent::login();
         if ($ret_val !== false) {
             static::updateLastTimestamp(['last_login_at', 'last_active_at'], $ret_val);
         }

         return $ret_val;
     }

     public function getUser()
     {
         return $this->user;
     }

    /**
     * Update activity timestamps for the user model. Specified values will be updated
     * with the 'NOW()' SQL expression.
     *
     * @param string|array $attribute The timestamp attributes to be updated
     * @param [type]       $user      [description]
     *
     * @return [type] [description]
     */
    public static function updateLastTimestamp($attribute, $user = null)
    {
        $user = $user instanceof User ? $user : \Yii::$app->getUser()->getIdentity();
        if ($user instanceof User) {
            //Not using SQL as storage medium may not be SQL database
            $timestamp = date('Y-m-d H:i:s', strtotime('now'));
            if (is_array($attribute)) {
                $user->setAttributes(array_combine($attribute, array_fill(0, count($attribute), $timestamp)));
            } else {
                $user->setAttribute($attribute, $timestamp);
            }
            $user->save();
        }
    }

     public static function updateLastLoggedIn($user = null)
     {
         static::updateLastTimestamp('last_login_at', $user);
     }

     public static function updateLastActive($user = null)
     {
         static::updateLastTimestamp('last_active_at', $user);
     }
 }
