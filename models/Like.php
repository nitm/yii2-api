<?php

namespace nitm\api\models;

/**
 * This is the model class for table "likes".
 *
 * @property int $id
 * @property int $item_id
 * @property string $item_type
 * @property int $user_id
 * @property string $created_at
 */
class Like extends UserAction
{
    protected $link = [
        'item_type' => 'item_type',
        'item_id' => 'item_id',
    ];
    protected $_rating;

    protected static $liked;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nitm_api_likes';
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), ['is_liked', 'is_disliked', 'like_count', 'dislike_count']);
    }

    public function getCreateAction()
    {
        return 'like';
    }

    public function getDeleteAction()
    {
        return 'dis-like';
    }

    /**
     * Get rating parameters.
     *
     * @method rating
     *
     * @param string|null $get         Get a specific part of the rating
     * @param bool        $reCalculate Force a recalculation
     *
     * @return mixed The rating value being sought
     */
    public function rating($get = null, $reCalculate = false)
    {
        if (isset($this->_rating) && $reCalculate === false) {
            $ret_val = $this->_rating;
        } else {
            $ret_val = ['positive' => 0, 'negative' => 0, 'ratio' => 0];
            static::$allowMultiple = false;
            $max = static::getMax($this);
            $ret_val = [
                'positive' => round(((int) $this->fetchedValue('is_liked') / $max) * 100),
                'negative' => round(((int) $this->fetchedValue('_down') / $max) * 100),
            ];
            $ret_val['ratio'] = round(abs($this->fetchedValue('_up') - abs($this->fetchedValue('_down'))) / $max, 2);
            $ret_val['max'] = $max;
            $this->_rating = $ret_val;
        }

        return ArrayHelper::getValue($ret_val, $get, $ret_val);
    }

    /**
     * Get the rating, percentage out of 100%.
     *
     * @return int
     */
    public static function getMax($vote = null)
    {
        //Need to make thismore efficient by using a cached user count value
        if (!isset(static::$maxLikes)) {
            static::$maxLikes = User::find()->count();
        }

        return (int) static::$maxLikes;
    }

    public function currentUserLike()
    {
        if (\Yii::$app->getUser()->isGuest) {
            return false;
        }

        $primaryKey = $this->primaryKey()[0];

        return static::find()
            ->select([
                '*',
                'is_disliked' => '(CASE value WHEN -1 THEN true ELSE false)',
                'is_liked' => '(CASE value WHEN 1 THEN true ELSE false)',
            ])
            ->andWhere(['user_id' => \Yii::$app->getUser()->getIdentity()->getId()])
            ->groupBy(array_keys($this->link));
    }

   /**
    * This is here to allow base classes to modify the query before finding the count.
    *
    * @return \yii\db\ActiveQuery
    */
   public function getFetchedValue()
   {
       $primaryKey = $this->primaryKey()[0];
       $ret_val = $this->hasOne(static::className(), $this->link);
       $valueFilter = @$this->queryOptions['value'];
       unset($this->queryOptions['andWhere']['value']);
       $select = [
           'is_disliked' => 'SUM(value=-1)',
           'is_liked' => 'SUM(value=1)',
       ];
       $filters = $this->queryOptions['andWhere'];
       unset($filters['item_id'], $filters['item_type']);

       return $ret_val->select(array_merge($this->link, $select))
           ->groupBy(array_keys($this->link))
           ->andWhere($filters);
   }

    public function fetchedValue($key = null)
    {
        $ret_val = \nitm\helpers\Relations::getRelatedRecord('fetchedValue', $this, static::className(), [
           'value' => 0,
           'is_liked' => 0,
           'is_disliked' => 0,
       ]);

        return ArrayHelper::getValue(ArrayHelper::toArray($ret_val), $key, $ret_val);
    }

    public function fields()
    {
        return [
                'id' => 'id',
                'type' => 'item_type',
                'object' => function ($model) {
                    return [$model->item_id];
                },
            ];
    }
}
