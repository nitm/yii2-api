<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace nitm\api\models;

use yii\base\Model;

/**
 * Registration form collects user input on registration process, validates it and creates new User model.
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class RegistrationForm extends \dektrium\user\models\RegistrationForm
{
    public $type;

    protected $_user;

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        array_push($scenarios['default'], 'username', 'type');

        return $scenarios;
    }

    public function register()
    {
        $result = parent::register();
        if ($result) {
            \Yii::$app->user->setIdentity($this->user);
        }

        return $result;
    }

    public function getUser()
    {
        if (!isset($this->_user)) {
            $user = \Yii::$app->user->identity;
            if (!$user) {
                $user = User::find()->where(['username' => $this->username])->one();
            }
            if ($user) {
                $this->_user = $user;
            }
        }

        return $this->_user;
    }
}
