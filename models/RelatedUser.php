<?php

namespace nitm\api\models;

use yii\base\Model;
use nitm\helpers\ArrayHelper;
use nitm\helpers\Cache;

/**
 * Rest User model adds some extra functionality.
 */
class User extends \nitm\models\User
{
    public function fields()
    {
        return [
            // field name is the same as the attribute name
            'id',
            'username',
            'email',
            'joined' => function ($model) {
                return \Yii::$app->formatter->asDate($model->created_at);
            },
            // field name is "email", the corresponding attribute name is "email_address",
            'avatar' => function ($model) {
                return $model->avatar();
            },
            // field name is "name", its value is defined by a PHP callback
            'fullName' => function ($model) {
                return $model->fullName();
            },
        ];
    }

    public function extraFields()
    {
        return [
            'profile' => function ($model) {
                $profile = ['id' => $model->username];
                $profile = array_merge($profile, ArrayHelper::toArray($model->profile));

                return [$profile];
            },
        ];
    }
}
