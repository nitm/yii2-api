<?php

namespace nitm\api\models;

use yii\base\Model;
use nitm\helpers\ArrayHelper;
use nitm\helpers\Cache;

/**
 * Rest User model adds some extra functionality.
 */
class User extends \nitm\models\User
{

    /**
     * @inheritdoc
     * @return [type] [description]
     */
    public function behaviors()
    {
        $behaviors = [
           'activity' => [
               'class' => \nitm\behaviors\Activity::class,
               'actionMap' => [
                   'create' => 'join'
               ]
           ]
        ];

        return array_merge(parent::behaviors(), $behaviors);
    }

    /** {@inheritdoc} */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $token = Token::find()->where(['token' => $token])->with('user')->one();
        if ($token) {
            if (!$token->user->apiToken) {
                $token->user->getApiToken()->one();
            }

            return $token->user;
        }

        return null;
    }

    public function getApiToken()
    {
        return $this->hasOne(Token::class, ['user_id' => 'id'])->inverseOf('user');
    }

    public function generateApiToken()
    {
        $token = new Token([
            'scenario' => 'create',
            'user_id' => $this->id,
        ]);
        $token->generate();
        $token->save();
        $this->populateRelation('apiToken', $token);

        return $token;
    }

    public function fields()
    {
        return [
            // field name is the same as the attribute name
            'id',
            'username',
            'email',
            'joined' => function ($model) {
                return \Yii::$app->formatter->asDate($model->created_at);
            },
            // field name is "email", the corresponding attribute name is "email_address",
            'avatar' => function ($model) {
                return $model->avatar();
            },
            // field name is "name", its value is defined by a PHP callback
            'fullName' => function ($model) {
                return $model->fullName();
            },
            'profile' => function ($model) {
                return $model->username;
            },
            'isConfirmed' => function ($model) {
                return $model->isConfirmed;
            },
            'role' => function ($model) {
                return $model->role ? $model->role->name : 'User';
            }
        ];
    }

    public function extraFields()
    {
        return [
            'profile' => function ($model) {
                $profile = ['id' => $model->username];
                $profile = array_merge($profile, ArrayHelper::toArray($model->profile));

                return [$profile];
            },
            'activity' => function ($model) {
                return $model->activity();
            }
        ];
    }
}
