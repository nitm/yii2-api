<?php

namespace nitm\api\models;

use Yii;
use nitm\filemanager\helpers\FileHelper;
use nitm\helpers\Cache;
use nitm\helpers\ClassHelper;
use yii\helpers\Inflector;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "nitm_api_page_config".
 *
 * @property int $id
 * @property string $page
 * @property string $class_name
 * @property string $config
 * @property int $author_id
 * @property string $created_at
 * @property string $deleted_at
 */
class PageConfig extends \nitm\db\ActiveRecord
{
    use \nitm\filemanager\traits\Relations;

    public $configType;
    public $configKey;
    public $configClass;
    public $configOptions;
    public $namespace;

    /**
     * Cache duration in seconds
     *
     * @var int
     */
    public static $duration = 1200;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nitm_api_page_config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page', 'config'], 'required'],
            [['author_id'], 'default', 'value' => null],
            [['author_id'], 'integer'],
            [['page'], 'unique', 'targetAttribute' => ['page']],
            [['created_at', 'deleted_at'], 'safe'],
            [['page', 'class_name'], 'string', 'max' => 64],
        ];
    }

    public function behaviors()
    {
        $behaviors = [
            'jsonable' => [
                'class' => \paulzi\jsonBehavior\JsonBehavior::class,
                'attributes' => [
                    'config'
                ]
            ]
        ];
        return array_replace_recursive(parent::behaviors(), $behaviors);
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'configKey', 'configType', 'configClass', 'configOptions'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'page' => Yii::t('app', 'Page'),
            'class_name' => Yii::t('app', 'Class Name'),
            'config' => Yii::t('app', 'Config'),
            'author_id' => Yii::t('app', 'Author ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    public function title()
    {
        return $this->model_name;
    }

    public static function getGroupName()
    {
        return 'PageConfig';
    }

    /**
     * @inheritdoc
     * @return \nitm\api\models\query\PageConfigQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \nitm\api\models\query\PageConfigQuery(get_called_class());
    }

    public function configParent($key)
    {
        $parts = explode('.', $key);
        array_pop($parts);
        return implode('.', $parts);
    }

    public function htmlParentId($key)
    {
        $parts = explode('.', $key);
        array_pop($parts);
        if (count($parts)) {
            return 'page-config-'.Inflector::slug(implode('.', $parts));
        }
        return 'page-config-list';
    }

    public function htmlId($key)
    {
        return 'page-config-'.Inflector::slug($key);
    }

    public function realKey($key)
    {
        return $key;
    }

    public function section($key)
    {
        $parts = explode('.', $key);
        return count($parts) ? array_shift($parts) : null;
    }

    public function toTree() {
        return $this->createTree(is_object($this->config) ? $this->config->toArray() : $this->config);
    }

    protected function createTree($models) {
        // $models = ArrayHelper::index($models, null, 'section');
        $ret_val = [];
        foreach($models as $groupName=>$value) {
            if(is_array($value)) {
                $section = $this->section($value['section']);
                $key = $value['key'];
                $parts = explode('.', $key);
                if(count($parts) > 1) {
                    array_shift($parts);
                    $key = implode('.', $parts);
                    $ret_val[$section][$key] = $this->createTree($value);
                } else {
                    $ret_val[$section][$key] = $value;
                }
            } else {
                $ret_val[$groupName] = $value;
            }
        }
        return $ret_val;
    }

    public function beforeSave($insert)
    {
        if($insert) {
            $page = preg_replace('/[^a-zA-Z0-9]/', '_', $this->page);
            $this->page = 'pageconfig'.preg_replace('/[^a-zA-Z0-9]/', '', strtolower($page));
            $namespace = ArrayHelper::getValue(\Yii::$app->params, 'rest-api.configModelNamespace', 'app\models');
            $this->model_name =  $this->getGroupName().Inflector::classify($page);
            $class = rtrim($namespace, '\\').'\\'.$this->model_name;
            $this->class_name = $class;
        }
        return parent::beforeSave($insert);
    }

    protected static function getPage($id)
    {
        $id = strpos($id, 'pageconfig') !== false ? $id : 'pageconfig'.$id;

        return static::find()->where(['page' => $id])->one();
    }

    /**
     * get the cache key for this model
     * @param  [type] $id     [description]
     * @param  [type] $params [description]
     * @return [type]         [description]
     */
    protected static function getCacheKey($id=null, $params=[])
    {
        $id = $id ?: strtolower(ClassHelper::getBasename(get_called_class()));
        return $id.'-'.md5(json_encode(array_merge($params, [
            md5(get_called_class()),
            $id,
        ])));
    }

    /**
     * Return the page configuration.
     *
     * @return array the configuration
     */
    public static function get($id = null, $options = [])
    {
        $id = $id ?: strtolower(\nitm\helpers\ClassHelper::getBasename(get_called_class()));
        $routeParams = $id != 'pageconfigglobal' ? \Yii::$app->controller->actionParams : [];
        $key = static::getCacheKey($id, array_merge((array)$_GET, (array)$routeParams));

        return Cache::remember($key, function () use ($id, $routeParams, $options) {
            $originalId = $id;
            $id = strtolower(preg_replace('/[^a-zA-Z0-9]/', '', $id));
            $page = static::getPage($id);
            if ($page) {
                $modelClass = $page->class_name;
                if (class_exists($modelClass)) {
                    /**
                     * We need to convert the found data into the relevant model
                     * @var $modelClass
                     */
                    $model = new $modelClass($page->attributes);
                    $model->config = $page->toTree();

                    $page = \Yii::$app->controller;
                    $args = [$page, $options, $routeParams];

                    $ret_val = array_merge([
                       'id' => $originalId,
                   ], (array) call_user_func_array([$model, 'prepareConfig'], $args));
                    return $ret_val;
                } else {
                    return [];
                }
            }
        }, false, static::$duration);
    }

    protected function getNavigation()
    {
        $ret_val = [];
        if (!$this->result || ($this->result && !in_array(get_class($this->result), [
            'Illuminate\Pagination\LengthAwarePaginator',
            'Illuminate\Pagination\Paginator',
        ]))) {
            return $ret_val;
        }
        if ($this->result->nextPageUrl()) {
            $ret_val['next'] = $this->result->nextPageUrl();
        }
        if ($this->result->previousPageUrl()) {
            $ret_val['previous'] = $this->result->previousPageUrl();
        }

        return $ret_val;
    }

    protected function getPagination($items = null)
    {
        $items = $items ?: $this->items;
        if ($items && !$items instanceof \Illuminate\Pagination\LengthAwarePaginator
            || $items instanceof \Illuminate\Pagination\Paginator) {
            return [];
        }

        return [
           'last' => $items->lastPage(),
           'next' => $items->currentPage() == $items->lastPage() ? $items->lastPage() : $items->currentPage() + 1,
           'previous' => $items->currentPage() == 1 ? 1 : $items->currentPage() - 1,
           'current' => $items->currentPage(),
        ];
    }

    public function getPageOptions()
    {
        $models = preg_grep('/^'.static::getGroupName().'(\w+).php/', scandir(__DIR__));
        $models = array_map(function ($model) {
            return substr($this, 0, strpos($model, '.'));
        }, $models);

        return array_combine(array_map('strtolower', $this), $this);
    }

    public function afterSave($insert, $changedAttributes=[])
    {
        if ($insert) {
            $modelCode = str_replace('%s', $this->model_name, $this->getPageModelTemplate());
            $modelPath = $this->modelPath;
            if (!FileHelper::exists($modelPath)) {
                FileHelper::writeFile($modelCode, $modelPath);
            }
        }
    }

    public function getPageName()
    {
        return substr($this->model_name, strlen($this->getGroupName()));
    }

    public function prepareConfig($controller, $config = [], $routeParameters = [])
    {
        throw new \Exception(__FUNCTION__.' needs to be defined by all sub classes');
    }

    public function getModel()
    {
        return $this;
    }

    protected function getModelPath()
    {
        return \Yii::getAlias('@'.$this->namespacedPath.'/'.$this->model_name.'.php');
    }

    /**
     * Lifted from RRainlab\Builder\Classes\ControllerGenerator.
     *
     * @param [type] $path [description]
     * @param [type] $data [description]
     *
     * @return [type] [description]
     */
    protected function writeFile($path, $data)
    {
        FileHelper::writeFile($data, $file);
    }

    public function getModelNamespace()
    {
        return ArrayHelper::getValue(\Yii::$app->params, 'rest-api.configModelNamespace', 'app\models');
    }

    protected function getNamespacedPath()
    {
        return (str_replace('\\', '/', $this->modelNamespace));
    }

    protected function getPageModelTemplate()
    {
        return "<?php namespace ".$this->modelNamespace.";

use Model;
use ".__CLASS__.";

/**
 * Model
 */
class %s extends PageConfig
{
}
";
    }
}
