<?php


namespace nitm\api\models;

use Yii;

/**
 * This is the model class for table "follows".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $network
 * @property boolean $is_member
 * @property string $created_at
 * @property boolean $deleted
 *
 * @property User $user
 */
class Follow extends Entity
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nitm_api_follows';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'network'], 'required'],
            [['user_id'], 'integer'],
            [['is_member', 'deleted'], 'boolean'],
            [['created_at'], 'safe'],
            [['network'], 'string', 'max' => 32],
            [['user_id', 'follow_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'network' => Yii::t('app', 'Network'),
            'is_member' => Yii::t('app', 'Is Member'),
            'created_at' => Yii::t('app', 'Created At'),
            'deleted' => Yii::t('app', 'Deleted'),
        ];
    }

    public function behaviors()
    {
        $behaviors = [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
                ],
                'value' => new \yii\db\Expression('NOW()')
            ],
        ];
        return $behaviors;
    }

    public function scenarios()
    {
        return [
            'create' => ['user_id', 'network_id', 'follow_id', 'network', 'is_member'],
            'update' => ['is_member', 'deleted']
        ];
    }
}
