<?php

namespace nitm\api\models;

/**
 * This is the model class for table "likes".
 *
 * @property int $id
 * @property int $item_id
 * @property string $item_type
 * @property int $user_id
 * @property string $created_at
 */
class Favorite extends UserAction
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nitm_api_favorites';
    }

    public function title()
    {
        return $this->item_id;
    }
}
