<?php
namespace nitm\api\models;

use yii\db\ActiveRecord;
use yii\helpers\Security;
use yii\web\IdentityInterface;
use nitm\models\Activity as ActivityBase;
use nitm\helpers\Cache;

/**
 * Class Profile
 * @package nitm\models
 */
class Activity extends ActivityBase
{
    /**
     * Get user relation.
     *
     * @param array $options Options for the relation
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser($options = [])
    {
        return $this->hasOne(RelatedUser::class, ['user_id' => 'id'])
            ->with(['profile', 'role']);
    }
}
