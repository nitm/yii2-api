<?php

namespace nitm\api\models\query;

/**
 * This is the ActiveQuery class for [[\nitm\api\models\PageConfig]].
 *
 * @see \nitm\api\models\PageConfig
 */
class PageConfigQuery extends \nitm\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \nitm\api\models\PageConfig[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \nitm\api\models\PageConfig|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
