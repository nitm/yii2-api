<?php

namespace nitm\api\models;

use Yii;

/**
 * This is the model class for table "likes".
 *
 * @property int $id
 * @property int $item_id
 * @property string $item_type
 * @property int $user_id
 * @property string $created_at
 */
class UserAction extends Entity
{
    protected $_item;

    public function attributes()
    {
        return array_merge(parent::attributes(), ['user_has_action']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_id', 'item_type', 'user_id', 'created_at'], 'required'],
            [['item_id', 'user_id'], 'integer'],
            [['created_at', 'item_id', 'item_type', 'deleted'], 'safe'],
            [['item_type'], 'string', 'max' => 32],
            [['item_id', 'item_type', 'user_id'], 'unique', 'targetAttribute' => ['item_id', 'item_type', 'user_id'], 'message' => 'You already '.static::isWhat().'d this!'],
        ];
    }

    public function behaviors()
    {
        $behaviors = [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'created_at',
                ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'search' => [
                'class' => \nitm\search\behaviors\SearchModel::class,
            ],
            'softDelete' => [
                'class' => \nitm\behaviors\SoftDelete::class,
            ],
            'activity' => [
                'class' => \nitm\behaviors\Activity::className(),
                'actionMap' => [
                    'create' => $this->createAction,
                    'delete' => $this->deleteAction,
                    'update' => $this->updateAction
                ]
            ]
        ];

        return $behaviors;
    }

    public static function has()
    {
        return ['user'];
    }

    public function scenarios()
    {
        return [
            'default' => [],
            'create' => ['item_id', 'item_type', 'user_id', 'value'],
            'update' => ['id', 'deleted', 'value'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'item_id' => Yii::t('app', 'Item ID'),
            'item_type' => Yii::t('app', 'Item Type'),
            'user_id' => Yii::t('app', 'User ID'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    public function fields()
    {
        return [
            'id',
            'item_type' => 'item_type',
            'item_id' => 'item_id',
            'title' => function ($model) {
                return $model->item()->title();
            },
            'item' => function ($model) {
                return $model->item()->toArray();
            },
        ];
    }

    public function getCreateAction()
    {
        return $this->is;
    }

    public function getUpdateAction()
    {
        return 'update-'.$this->is;
    }

    public function getDeleteAction()
    {
        return 'un-'.$this->is;
    }

    public function extraFields()
    {
        return [
        ];
    }

    public function getItem()
    {
        if (!isset($this->_item)) {
            $this->_item = $this->getItemQuery()->one();
        }
        return $this->_item;
    }

    public function getItemQuery()
    {
        $class = $this->item_class;
        if (class_exists($this->item_class)) {
            return $class::find()->where(['id' => $this->item_id]);
        } else {
            return Entity::find();
        }
    }

    public function getItemType()
    {
        return $this->item_type;
    }

    public function beforeSave($insert, $changedAttributes=[])
    {
        if ($insert) {
            $itemClass = $this->item_class ?: __NAMESPACE__.'/'.$this->item_type;
            if (class_exists($itemClass)) {
                $this->item_table = $itemClass::tableName();
            }
        }
        return parent::beforeSave($insert, $changedAttributes);
    }
}
