<?php
namespace nitm\api\models;

use yii\db\ActiveRecord;
use yii\helpers\Security;
use yii\web\IdentityInterface;
use nitm\models\Profile as UserProfile;
use nitm\helpers\Cache;

/**
 * Class Profile
 * @package nitm\models
 */
class Profile extends UserProfile
{
}
