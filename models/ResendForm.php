<?php

namespace nitm\api\models;

use dektrium\user\Finder;
use dektrium\user\Mailer;
use yii\base\Model;

/**
 * ResendForm gets user email address and if user with given email is registered it sends new confirmation message
 * to him in case he did not validate his email.
 *
 * @author Malcolm paul <malcolm@ninjasitm.com>
 */
class ResendForm extends \dektrium\user\models\ResendForm
{
}
