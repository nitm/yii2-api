<?php

//Some fixes for broken aliases
\Yii::setAlias('@nitm', dirname(__DIR__).'/../../nitm');
\Yii::setAlias('@dektrium/user', dirname(__DIR__).'/../../dektrium/yii2-user');
\Yii::setAlias('@dektrium/rbac', dirname(__DIR__).'/../../dektrium/yii2-rbac');

return [
    'id' => 'nitm-tests',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['user', 'nitm', 'rest-api'],
    'params' => [
      'yii.migrations' => [
      ]
    ],
    'controllerMap' => [
      'migrate' => [
          'class' => 'dmstr\console\controllers\MigrateController'
        ]
    ],
    'components' => [
        'request' => [
          // 'class' => \yii\web\Request::class,
            // 'enableCookieValidation' => false,
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            // 'cookieValidationKey' => str_random(32),
        ],
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'nitm\api\models\User',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            // uncomment if you want to cache RBAC items hierarchy
            // 'cache' => 'cache',
        ],
        'i18n' => [
            'translations' => [
                'user' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@dektrium/user/messages',
                    'sourceLanguage' => 'en-US',
                ],
            ],
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => "sqlite:memory",
        ]
    ],
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
        ],
        'nitm' => [
            'class' => \nitm\Module::className()
        ],
        'rest-api' => [
            'class' => \nitm\api\Module::className()
        ],
    ]
];
