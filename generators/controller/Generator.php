<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace nitm\api\generators\controller;

use Yii;
use yii\gii\CodeFile;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/**
 * This generator will generate a controller and one or a few action view files.
 *
 * @property array $actionIDs An array of action IDs entered by the user. This property is read-only.
 * @property string $controllerFile The controller class file path. This property is read-only.
 * @property string $controllerID The controller ID. This property is read-only.
 * @property string $controllerNamespace The namespace of the controller class. This property is read-only.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Generator extends \yii\gii\generators\controller\Generator
{
    /**
     * @var string the controller class name
     */
    public $modelClass;
    public $baseClass = 'nitm\api\controllers\BaseActiveApiController';
    public $overwriteActions = false;

    protected $_actionSuccessMessage;
    protected $_actionCode;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['controllerClass', 'modelClass', 'actions', 'baseClass'], 'filter', 'filter' => 'trim'],
            [['controllerClass', 'baseClass', 'modelClass'], 'required'],
            ['controllerClass', 'match', 'pattern' => '/^[\w\\\\]*Controller$/', 'message' => 'Only word characters and backslashes are allowed, and the class name must end with "Controller".'],
            ['controllerClass', 'validateNewClass'],
            ['controllerClass', 'filter', 'filter' => [$this, 'trimControllerNamespacePath']],
            ['baseClass', 'match', 'pattern' => '/^[\w\\\\]*$/', 'message' => 'Only word characters and backslashes are allowed.'],
            ['actions', 'match', 'pattern' => '/^[a-z][a-z0-9\\-,\\s]*$/', 'message' => 'Only a-z, 0-9, dashes (-), spaces and commas are allowed.'],
            ['viewPath', 'safe'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'baseClass' => 'Base Class',
            'modelClass' => 'Model Class',
            'controllerClass' => 'Controller Class',
            'viewPath' => 'View Path',
            'actions' => 'Action IDs',
        ];
    }

    /**
     * @inheritdoc
     */
    public function stickyAttributes()
    {
        return ['baseClass', 'modelClass'];
    }

    /**
     * @inheritdoc
     */
    public function hints()
    {
        return [
            'controllerClass' => 'This is the name of the controller class to be generated. You should
                provide a fully qualified namespaced class (e.g. <code>app\controllers\PostController</code>),
                and class name should be in CamelCase ending with the word <code>Controller</code>. Make sure the class
                is using the same namespace as specified by your application\'s controllerNamespace property.',
            'actions' => 'Provide one or multiple action IDs to generate empty action method(s) in the controller. Separate multiple action IDs with commas or spaces.
                Action IDs should be in lower case. For example:
                <ul>
                    <li><code>index</code> generates <code>actionIndex()</code></li>
                    <li><code>create-order</code> generates <code>actionCreateOrder()</code></li>
                </ul>',
            'viewPath' => 'Specify the directory for storing the view scripts for the controller. You may use path alias here, e.g.,
                <code>/var/www/basic/controllers/views/order</code>, <code>@app/views/order</code>. If not set, it will default
                to <code>@app/views/ControllerID</code>',
            'baseClass' => 'This is the class that the new controller class will extend from. Please make sure the class exists and can be autoloaded.',
            'modelClass' => "This is the class class for the controller's model.",
            'overwriteActions' => "If you check this then existing actions will be overwritten",
        ];
    }

    /**
     * @inheritdoc
     */
    public function successMessage()
    {
        return "The controller has been generated successfully."."<br>".$this->_actionSuccessMessage;
    }

    public function trimControllerNamespacePath($controllerClass)
    {
        $controllerClass = trim($controllerClass, '\\');
        return $controllerClass;
    }

    /**
     * @inheritdoc
     */
    public function generate()
    {
        $files = [];

        if ($this->overwriteActions) {
            $actionGenerator = new \nitm\api\generators\action\Generator([
                'actions' => implode(',', $this->getActionIDs()),
                'modelClass' => $this->modelClass,
                'namespace' => $this->getControllerNamespace(),
                'controllerId' => $this->getControllerID(),
                'reallyGenerate' => $this->overwriteActions
            ]);

            $files = array_merge($files, $actionGenerator->generate());
            $this->_actionSuccessMessage = $actionGenerator->successMessage();

            $this->_actionCode = $actionGenerator->getActionContentForContoller();
        }

        $files[] = new CodeFile(
            $this->getControllerFile(),
            $this->render('controller.php')
        );

        return $files;
    }

    public function getActionCode()
    {
        return $this->_actionCode;
    }
}
