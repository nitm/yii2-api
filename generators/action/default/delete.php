<?php
/**
 * This is the template for generating a controller class file.
 */

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\controller\Generator */

echo "<?php\n";
?>

namespace <?= $generator->getActionNamespace()?>\delete;

use <?= $baseClass ?> as BaseAction;

class <?= $actionName ?> extends BaseAction <?= "\n" ?>
{
    public $modelClass = '<?= $modelClass ?>';

    /**
     * Delete <?= $modelClass ?> model
     * @return DataProvider
     */
    public function run($id)
    {
        return parent::run($id);
    }
}
