<?php
/**
 * This is the template for generating a controller class file.
 */

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\controller\Generator */

echo "<?php\n";
?>

namespace <?= $generator->getActionNamespace()?>\update;

use <?= $baseClass ?> as BaseAction;

class <?= $actionName ?> extends BaseAction <?= "\n" ?>
{
    public $modelClass = '<?= $modelClass ?>';

    /**
     * Update <?= $modelClass ?> model
     * @return DataProvider
     */
    public function run($id)
    {
        //Custom logic here
        return parent::run($id);
    }

    public function checkAccess()
    {
        //Custom access checking code here. Remove if parent handles this code properly
    }
}
