<?php
/**
 * This is the template for generating a controller class file.
 */

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\controller\Generator */

echo "<?php\n";
?>

namespace <?= $generator->getActionNamespace()?>\view;

use <?= $baseClass ?> as BaseAction;

class <?= $actionName ?> extends BaseAction <?= "\n" ?>
{
    public $modelClass = '<?= $modelClass ?>';
}
