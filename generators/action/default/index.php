<?php
/**
 * This is the template for generating a controller class file.
 */

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\controller\Generator */

echo "<?php\n";
?>

namespace <?= $generator->getActionNamespace()?>\index;

use <?= $baseClass ?> as BaseAction;

class <?= $actionName ?> extends BaseAction <?= "\n" ?>
{
    public $modelClass = '<?= $modelClass ?>';

    /**
     * Lists all <?= $modelClass ?> models
     * @return DataProvider
     */
    public function prepareDataProvider($modelClass=null, $options=[], $searchOptions=[], $cache=true)
    {
        $searchOptions = array_merge($searchOptions, [
            //'queryOptions' => [
            //    'with' => [],
            //    'select' => ['*']
            //]
        ]);
        return parent::prepareDataProvider($modelClass, $options, $searchOptions, $cache);
    }
}
