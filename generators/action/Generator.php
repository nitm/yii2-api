<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace nitm\api\generators\action;

use yii\gii\CodeFile;
use yii\helpers\Html;
use yii\helpers\Inflector;
use Yii;
use yii\helpers\StringHelper;

/**
 * This generator will generate the skeleton code needed by a action.
 *
 * @property string $controllerNamespace The controller namespace of the action. This property is read-only.
 * @property boolean $actionPath The directory that contains the action class. This property is read-only.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Generator extends \yii\gii\Generator
{
    public $namespace;
    public $controllerId;
    public $actions;
    public $modelClass;
    public $reallyGenerate;

    protected $_generatedActions = [];


    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Action Generator';
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return 'This generator helps you to generate the skeleton code needed by an nitm/api action.';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['actions', 'namespace', 'controllerId', 'modelClass'], 'filter', 'filter' => 'trim'],
            [['actions', 'namespace', 'controllerId', 'modelClass'], 'required'],
            [['actions', 'controllerId'], 'match', 'pattern' => '/^[\s\,\w\\-]+$/', 'message' => 'Only word characters, commas and dashes are allowed.'],
            [['namespace'], 'match', 'pattern' => '/^[\,\w\\\\]*$/', 'message' => 'Only word characters and backslashes are allowed.'],
            [['namespace'], 'validateModuleClass'],
            [['modelClass'], 'validateModelClass'],
            [['actions'], 'validateSupportedActions'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'actions' => 'Actions',
            'namespace' => 'Namesapce',
        ];
    }

    /**
     * @inheritdoc
     */
    public function hints()
    {
        return [
            'actions' => 'This refers to the ID of the action, e.g., <code>index,create,...etc</code>.',
            'namespace' => 'This is the basename space of the actions, e.g., <code>app\actions</code>.',
        ];
    }

    /**
     * @inheritdoc
     */
    public function successMessage()
    {
        $output = <<<EOD
<p>The actions have been generated successfully.</p>
<p>To access the action, you need to add this to your controller actions method:</p>
EOD;
        $actionCode = $this->getActionContentForContoller();
        if ($actionCode) {
            $code = <<<EOD
<?php
    ......
    $actionCode;
    ......
EOD;
        }

        return $output . '<pre>' . highlight_string($code, true) . '</pre>';
    }

    /**
     * @inheritdoc
     */
    public function requiredTemplates()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function generate()
    {
        $files = [];
        $actionPath = $this->getModulePath();
        foreach ($this->getActionIDs() as $action) {
            $action = trim($action);
            $baseAction = Inflector::classify($action);
            $actionName = Inflector::classify($this->controllerId.'Action');
            $baseClass = 'nitm\api\actions\\'.$baseAction;
            $actionClass = $this->actionNamespace.'\\'.$action.'\\'.$actionName;
            if (!$this->reallyGenerate) {
                $files[] = new CodeFile(
                $actionPath . '/actions/' .$action.'/'. $actionName . '.php',
                $this->render("$action.php", [
                        'actionName' => $actionName,
                        'baseClass' => $baseClass,
                        'baseAction' => $baseAction,
                        'modelClass' => $this->modelClass
                    ])
                );
            }
            $this->_generatedActions[$action] = $actionClass;
        }

        return $files;
    }

    /**
     * Validates [[namespace]] to make sure it is a fully qualified class name.
     */
    public function validateModuleClass()
    {
        if (strpos($this->namespace, '\\') === false || Yii::getAlias('@' . str_replace('\\', '/', $this->namespace), false) === false) {
            $this->addError('namespace', 'Action class must be properly namespaced.');
        }
        if (empty($this->namespace) || substr_compare($this->namespace, '\\', -1, 1) === 0) {
            $this->addError('namespace', 'Action namesapce name must not be empty. Please enter a fully qualified action namespace. e.g. "app\\actions\\".');
        }
    }

    public function validateModelClass()
    {
        if (!class_exists($this->modelClass)) {
            $this->addError('modelClass', $this->modelClass." does not exist!");
        }
    }

    public function validateSupportedActions()
    {
        $supported = ['index', 'create', 'update', 'delete', 'view'];
        $diff = array_diff($this->getActionIDs(), $supported);
        if (count($diff)) {
            $this->addError('actions', "The following actions are not supported: [".implode(', ', $diff)."]. Only the following actions are supported: [".implode(', ', $supported)."]");
        }
    }

    public function getActionContentForContoller()
    {
        if (count($this->_generatedActions)) {
            $actionString = '';
            $counter = 0;
            foreach ($this->_generatedActions as $action=>$class) {
                $tabs = $counter ? "\t\t" : "";
                $actionString .= $tabs."\$actions['".$action."'] = \\$class::class;\n";
                $counter++;
            }
            $code = <<<EOD
    public function actions() {
        \$actions = parent::actions();
        $actionString
        return \$actions;
    }

EOD;
            return $code;
        } else {
            return '';
        }
    }

    /**
     * Normalizes [[actions]] into an array of action IDs.
     * @return array an array of action IDs entered by the user
     */
    public function getActionIDs()
    {
        $actions = array_unique(preg_split('/[\s,]+/', $this->actions, -1, PREG_SPLIT_NO_EMPTY));
        sort($actions);

        return $actions;
    }

    /**
     * @return boolean the directory that contains the action class
     */
    public function getModulePath()
    {
        return Yii::getAlias('@' . str_replace('\\', '/', substr($this->namespace, 0, strrpos($this->namespace, '\\'))));
    }

    /**
     * @return string the controller namespace of the action.
     */
    public function getActionNamespace()
    {
        return substr($this->namespace, 0, strrpos($this->namespace, '\\')) . '\actions';
    }
}
