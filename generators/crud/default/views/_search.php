<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator nitm\generators\crud\Generator */

echo "<?php\n";
?>

use nitm\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->searchModelClass, '\\') ?> */
/* @var $form yii\widgets\ActiveForm */

$formClass = \Yii::$container->get('kartik\widgets\ActiveForm');

$defaultFormOptions = [
    'type' => 'vertical',
    'action' => ['index'],
    'method' => 'get'
];
$formOptions = isset($formOptions) ? array_merge($defaultFormOptions, $formOptions) : $defaultFormOptions;
$formOptions['options']['role'] = 'ajaxForm';

?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-search">

    <?= "<?php " ?>$form = include(\Yii::getAlias("@nitm/views/layouts/form/header.php")); ?>

<?php
$count = 0;
foreach ($generator->getColumnNames() as $attribute) {
    if (++$count < 6) {
        echo "    <?= " . $generator->generateActiveSearchField($attribute) . " ?>\n\n";
    } else {
        echo "    <?php // echo " . $generator->generateActiveSearchField($attribute) . " ?>\n\n";
    }
}
?>
    <div class="form-group">
        <?= "<?= " ?>Html::submitButton(<?= $generator->generateString('Search') ?>, ['class' => 'btn btn-primary']) ?>
        <?= "<?= " ?>Html::resetButton(<?= $generator->generateString('Reset') ?>, ['class' => 'btn btn-default']) ?>
    </div>

    <?= "<?php " ?>$form->end(); ?>

</div>
