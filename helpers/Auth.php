<?php

namespace nitm\api\helpers;

use nitm\helpers\ArrayHelper;

class Auth extends \yii\base\BaseObject
{
    public static function getBaseUrl($params = null, $type = 'website')
    {
        $params = is_array($params) ? $params : [];
        if (\Yii::$app && $params == []) {
            $params = \Yii::$app->params;
        }
        $host = ArrayHelper::getValue(\Yii::$app->getModule('rest-api')->socialAuthOptions, 'urls.'.$type, 'https://'.$_SERVER['SERVER_NAME']);
        $host = empty($host) ? 'https://'.$_SERVER['SERVER_NAME'] : $host;
        $host = is_callable($host) ? call_user_func($host) : $host;

        return $host;
    }

    public static function getArgs($args = null)
    {
        if (empty($args)) {
            $args = array_intersect_key($_GET, array_flip([
                'authclient', 'from',
            ]));
        }
        ksort($args);

        return http_build_query($args);
    }

    protected function getUrl($params, $type, $for, $args = [])
    {
        $route = trim($for, '/');
        $route = ArrayHelper::getValue(\Yii::$app->getModule('rest-api')->socialAuthOptions, 'routesOn.'.$for, $for);

        return self::getBaseUrl($params, $type).'/'.$route.'?'.self::getArgs($args);
    }

    public static function getConnectUrl($params, $type = 'website')
    {
        $args = array_intersect_key($_GET, array_flip([
            'from',
        ]));
        if (is_array($args) && is_array($params)) {
            $args += $params;
        } else {
            $args = is_array($args) ? $args : $params;
        }

        return self::getUrl(null, $type, 'connect', $args);
    }

    public static function getAuthReferer($params = null, $type = 'website')
    {
        return self::getUrl($params, $type, 'auth');
    }

    public static function getLoginReferer($params = null)
    {
        return self::getUrl($params, 'website', 'login');
    }

    public static function getReturnUrl($params = null, $type = 'website')
    {
        if (($url = \Yii::$app->getSession()->get('auth-return-url')) != null) {
            \Yii::$app->getSession()->remove('auth-return-url');
        } else {
            $url = self::getUrl($params, $type, 'return');
        }

        return $url;
    }

    public static function setReturnUrl()
    {
        $returnUrl = \Yii::$app->request->get('r');
        if (!empty($returnUrl)) {
            \Yii::$app->getSession()->set('auth-return-url', $returnUrl);
        }
    }

    public function getToken($account)
    {
        $collection = \Yii::$app->get('authClientCollection');
        $client = null;
        if ($collection) {
            $client = $collection->hasClient($account->provider) ? $collection->getClient($account->provider) : null;
        }
        if (is_null($client)) {
            return null;
        }
        if ($client->getAccessToken()) {
            return $client->getAccessToken()->getToken();
        } else {
            return null;
        }
    }
}
