<?php
namespace nitm\api\filters\auth;

use yii\filters\auth\QueryParamAuth as BaseQueryParamsAuth;
use nitm\api\models\Token;

/**
 * Custom query param auth based on yii query param auth
 * @author malcolm@ninjasitm.com
 */
class QueryParamAuth extends BaseQueryParamsAuth
{
    /**
     * @inheritdoc
     */
    public function authenticate($user, $request, $response)
    {
        $accessToken = $request->get($this->tokenParam);
        if (is_string($accessToken)) {
            $token = \nitm\helpers\Cache::remember(['api-token', $accessToken], function () use ($accessToken) {
                $token = Token::findByKey($accessToken);
                if ($token) {
                    return $token;
                }
                return null;
            }, 3600);
            if ($token) {
                if ($token->isExpired) {
                    $token->regenerate();
                }
                $identity = $user->loginByAccessToken($token->token, get_class($this));
            }
            if (isset($identity) && $identity !== null) {
                return $identity;
            }
        }
        // if ($accessToken !== null) {
        //     $this->handleFailure($response);
        // }
        return null;
    }
}
