<?php
/**
 * @link http://www.nitm\api.com/
 *
 * @copyright Copyright (c) 2015 PickledUp Inc
 */

namespace nitm\api;

use yii\base\Arrayable;
use yii\base\Model;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\web\Link;
use yii\web\Request;

/**
 * Custom serializer based on \yii\rest\Serializer.
 *
 * @author Malcolm Paul <malcolm@ninjasitm.com>
 */
class Serializer extends \yii\rest\Serializer
{
    /**
     * @var string the name of the envelope (e.g. `_links`) for returning the links objects.
     *             It takes effect only, if `collectionEnvelope` is set
     *
     * @since 2.0.4
     */
    public $linksEnvelope = 'links';
    /**
     * @var string the name of the envelope (e.g. `_meta`) for returning the pagination object.
     *             It takes effect only, if `collectionEnvelope` is set
     *
     * @since 2.0.4
     */
    public $metaEnvelope = 'meta';

    /**
     * Serializes a set of models.
     *
     * @param array $models
     *
     * @return array the array representation of the models
     */
    protected function serializeModels(array $models)
    {
        list($fields, $expand) = $this->getRequestedFields();
        foreach ($models as $i => $model) {
            $models[$i] = $this->serializeModel($model)[$model->is];
        }

        return $models;
    }

    /**
     * Serializes a model object.
     *
     * @param Arrayable $model
     *
     * @return array the array representation of the model
     */
    protected function serializeModel($model)
    {
        if ($this->request->getIsHead()) {
            return null;
        } elseif (is_object($model)) {
            list($fields, $expand) = $this->getRequestedFields();
            if ($fields == []) {
                list($fields, $extraFields, $allFields) = $model->allFields();
            } else {
                list($fields, $extraFields, $allFields) = $model->allFields();
            }
            $expand = empty($expand) ? $extraFields : $expand;
            $attributes = $model->toArray($fields, $expand);
            $ret_val = $this->normalizeForApi($attributes, $model->isWhat(), [$fields, $extraFields, $allFields]);

            return $ret_val;
        } else {
            $modelClass = \Yii::$app->controller->modelClass;

            return $this->normalizeForApi($model, $modelClass::isWhat(), $modelClass::allFields());
        }
    }

    /**
     * Format the model properly for the API.
     *
     * @param array      $row   The array attributes
     * @param BaseSearch $model THe search model
     *
     * @return array The normalized data
     */
    public function normalizeForApi($properties, $type = null, $modelFields = [])
    {
        if (empty($modelFields)) {
            $modelClass = \Yii::$app->controller->modelClass;
            $model = new $modelClass();
            $modelFields = $model->allFields($modelClass);
        }

        return $this->organize($properties, $type, $modelFields);
    }

    protected function organize($properties, $type, $modelFields)
    {
        list($fields, $extraFields, $allFields) = $modelFields;
        $ret_val = [];
        $properties = $this->normalize($type, $properties);
        foreach ($properties as $k => $v) {
            if (is_null($v)) {
                continue;
            }
            //We have a global relation that other models may also have

            if (($options = static::normalizerFor($k)) !== false) {
                $v = call_user_func_array([$this, 'normalizeSpecial'], [$v, $options]);
            }
            $ret_val[$type][$k] = $this->normalize($k, $v);
        }

        return $ret_val;
    }

    protected function normalize($k, $v)
    {
        if (($targetClass = static::normalizerFor($k)) !== false) {
            $v = call_user_func_array([$this, 'normalizeSpecial'], [$v, $targetClass]);
        }

        return $v;
    }

    /**
     * Normalize document data. This data is generally pre formated and setup.
     *
     * @param array      $row         [description]
     * @param BaseSearch $model       Search model
     * @param array      $modelFields The fields for the document
     *
     * @return array THe normalized data
     */
    protected function normalizeDocumentForApi($row, $model, $modelFields)
    {
        list($fields, $extraFields, $allFields) = $modelFields;
        $array = array_intersect_key($row, array_flip($allFields));
        //Remove unecessary keys from row data
        // foreach ([
        //     'approved'
        // ] as $toRemove) {
        //     unset($array[$toRemove]);
        // }
        return static::organizeForApi($properties, $model, $modelFields);
    }

    /**
     * Normalize special data.
     *
     * @param mixed      $value   The value tobe normalized
     * @param array      $options Optios for normalizing the value
     * @param array|null $fields  All of the fields for the model/class
     *
     * @return mixed Normalized special value
     */
    protected function normalizeSpecial($value, $options = [], $fields = null)
    {
        $class = ArrayHelper::getValue($options, 'class', static::className());
        $normalizer = ArrayHelper::getValue($options, 'normalizer', null);
        if ($normalizer && method_exists(static::className(), $normalizer)) {
            $value = call_user_func_array([static::className(), $normalizer], [$value]);
        }
        if (!is_array($fields)) {
            list($f, $e, $fields) = $class::allFields($class);
        }
        if (ArrayHelper::isIndexed($value)) {
            return array_map(function ($v) use ($fields) {
                if (is_array($v)) {
                    return array_intersect_key($v, array_flip($fields));
                }

                return $v;
            }, $value);
        } else {
            if (is_array($value)) {
                return array_intersect_key($value, array_flip($fields));
            } else {
                return $value;
            }
        }
    }

    protected function normalizeUser($value)
    {
        if (ArrayHelper::isIndexed($value)) {
            $value = array_map(function ($user) {
                if (isset($value['id'])) {
                    $user['id'] = $user['username'];
                }

                return $user;
            }, $value);
        } elseif (isset($value['id'])) {
            if (isset($value['username'])) {
                $value['id'] = $value['username'];
            }
        }

        return $value;
    }

    protected function normalizerFor($for)
    {
        $ret_val = false;
        switch (\yii\helpers\Inflector::singularize($for)) {
            case 'editor':
            case 'author':
            case 'user':
            $ret_val = [
                'class' => \Yii::$app->user->identityClass,
                'normalizer' => 'normalizeUser',
            ];
            break;
        }

        return $ret_val;
    }
}
