<?php
/**
 * @link http://www.nitm\api.com/
 *
 * @copyright Copyright (c) 2015 PickledUp Inc
 */

namespace nitm\api;

use yii\base\Arrayable;
use yii\base\Model;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\web\Link;
use yii\web\Request;

/**
 * Custom serializer based on \yii\rest\Serializer.
 * Adheres to http://jsonapi.org/ JSON API description.
 *
 * @author Malcolm Paul <lefteyecc@nitm\api.com>
 */
class JSONAPISerializer extends \yii\rest\Serializer
{
    public $collectionEnvelope = 'data';
    /**
     * @var string the name of the envelope (e.g. `_links`) for returning the links objects.
     *             It takes effect only, if `collectionEnvelope` is set
     *
     * @since 2.0.4
     */
    public $linksEnvelope = 'links';
    /**
     * @var string the name of the envelope (e.g. `_meta`) for returning the pagination object.
     *             It takes effect only, if `collectionEnvelope` is set
     *
     * @since 2.0.4
     */
    public $metaEnvelope = 'meta';

    /**
     * Serializes a model object.
     *
     * @param Arrayable $model
     *
     * @return array the array representation of the model
     */
    protected function serializeModel($model)
    {
        if ($this->request->getIsHead()) {
            return null;
        } else {
            list($fields, $expand) = $this->getRequestedFields();
            $attributes = $model->toArray($fields);
            $ret_val = [
                'type' => $model->isWhat(),
                'id' => ArrayHelper::getValue($attributes, 'id', uniqid()),
                'attributes' => $attributes,
            ];
            if ($expand) {
                //We use an array with 0 as only property to allow for fetching relationships in extra fields
                $ret_val['relationships'] = $model->toArray([0], $expand);
            }

            return $ret_val;
        }
    }
}
