<?php

use nitm\api\tests\ApiTester;

class UserCest
{
    public function _before(ApiTester $I)
    {
    }

    public function _after(ApiTester $I)
    {
    }

    //Test that a member can register
    public function registerTest(ApiTester $I)
    {
        $I->wantTo('Register a new member via the API');

        $I->haveHttpHeader('Content-Type', 'application/json');

        $I->sendPOST('registration/register', [
            'email'         => 'random@email.com',
            'password'      => 'Test1234',
        ]);

        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
    }

    // tests
    public function createUserViaAPI(ApiTester $I)
    {
        $I->amHttpAuthenticated('service_user', '123456');
        $I->haveHttpHeader('Content-Type', 'application/x-www-form-urlencoded');
        $I->sendPOST('user', ['name' => 'davert', 'email' => 'davert@codeception.com']);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"result":"ok"}');
    }
}
