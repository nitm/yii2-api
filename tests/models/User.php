<?php

namespace nitm\api\tests\models;

use yii\helpers\Security;
use nitm\helpers\Cache;
use nitm\helpers\ArrayHelper;

/**
 * Class User.
 *
 * @property int $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $token
 * @property string $email
 * @property string $auth_key
 * @property int $role
 * @property int $status
 * @property int $create_time
 * @property int $update_time
 * @property string $password write-only password
 */
class User extends \nitm\tests\models\User
{
}
