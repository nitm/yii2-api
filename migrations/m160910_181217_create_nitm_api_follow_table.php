<?php

use yii\db\Migration;

/**
 * Handles the creation of table `follow`.
 */
class m160910_181217_create_nitm_api_follow_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeup()
    {
        $table = \nitm\api\models\Follow::tableName();
        $tableSchema = \Yii::$app->db->getTableSchema($table);
        if ($tableSchema) {
            return true;
        }
        $this->createTable($table, [
            'id' => $this->primaryKey(),
            'item_id' => $this->integer()->notNull(),
            'item_table' => $this->string('64'),
            'item_type' => $this->string('64')->notNull(),
            'item_class' => $this->text(),
            'user_id' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->defaultValue('NOW()'),
            'deleted_at' => $this->timestamp(),
        ]);

        //These Dbs don't support foreign keys
        if(in_array(get_class($this->db->schema), [
          \yii\db\sqlite\Schema::class
        ])) {
          return;
        }

        $this->addForeignKey('fk_follow_user', '{{'.$table.'}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * {@inheritdoc}
     */
    public function safedown()
    {
        $table = \wukdo\models\Follow::tableName();
        $this->dropTable($table);
    }
}
