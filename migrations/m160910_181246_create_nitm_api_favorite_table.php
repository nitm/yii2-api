<?php

use yii\db\Migration;

/**
 * Handles the creation of table `nitm_api_favorite`.
 */
class m160910_181246_create_nitm_api_favorite_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeup()
    {
        $table = \nitm\api\models\Favorite::tableName();
        $tableSchema = \Yii::$app->db->getTableSchema($table);
        if ($tableSchema) {
            return true;
        }
        $this->createTable($table, [
            'id' => $this->primaryKey(),
            'item_id' => $this->integer()->notNull(),
            'item_table' => $this->string('64'),
            'item_type' => $this->string('64')->notNull(),
            'item_class' => $this->text(),
            'user_id' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->defaultValue('NOW()'),
            'deleted_at' => $this->timestamp(),
        ]);

        //These Dbs don't support foreign keys
        if(in_array(get_class($this->db->schema), [
          \yii\db\sqlite\Schema::class
        ])) {
          return;
        }
        $this->addForeignKey('fk_favorite_user', '{{'.$table.'}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * {@inheritdoc}
     */
    public function safedown()
    {
        $table = \wukdo\models\Favorite::tableName();
        $this->dropTable($table);
    }
}
