<?php

use yii\db\Migration;

/**
 * Handles the creation of table `token`.
 */
class m161023_191334_create_token_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableSchema = \Yii::$app->db->getTableSchema('nitm_api_token');
        if ($tableSchema) {
            return true;
        }
        $this->createTable('nitm_api_token', [
            'id' => $this->primaryKey(),
            'token' => $this->string(),
            'user_id' => $this->integer(),
            'is_permanent' => $this->boolean()->null(),
            'expires_at' => $this->timestamp()->null()
        ]);

        $this->createIndex('unique_token', 'nitm_api_token', ['user_id'], true);

        //These Dbs don't support foreign keys
        if(in_array(get_class($this->db->schema), [
          \yii\db\sqlite\Schema::class
        ])) {
          return;
        }
        $this->addForeignKey('fk_user', '{{%nitm_api_token}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
         $this->dropTable('token');
    }
}
