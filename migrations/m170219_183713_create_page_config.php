<?php

use yii\db\Migration;

class m170219_183713_create_page_config extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $tableSchema = \Yii::$app->db->getTableSchema('nitm_api_page_config');
        if ($tableSchema) {
            return true;
        }
        $this->createTable('nitm_api_page_config', [
            'id' => $this->primaryKey(),
            'page' => $this->string(64)->notNull(),
            'class_name' => $this->text(),
            'model_name' => $this->string(64)->notNull(),
            'config' => 'json NULL',
            'author_id' => $this->integer(),
            'created_at' => $this->timestamp()->defaultValue('NOW()'),
            'deleted_at' => $this->timestamp(),
        ]);

        // creates index for column `author_id`
        $this->createIndex(
            'idx-post-author_id',
            'nitm_api_page_config',
            ['page', 'class_name']
        );
    }

    public function safeDown()
    {
        $this->dropTable('nitm_api_page_config');
    }
}
