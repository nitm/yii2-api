<?php

use yii\db\Migration;

/**
 * Handles the creation of table `nitm_api_friend`.
 */
class m160910_181231_create_nitm_api_friend_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeup()
    {
        $table = \nitm\api\models\Friend::tableName();
        $tableSchema = \Yii::$app->db->getTableSchema($table);
        if ($tableSchema) {
            return true;
        }
        $this->createTable($table, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'friend_id' => $this->integer(),
            'is_member' => $this->boolean()->defaultValue(false),
            'network' => $this->string('32'),
            'created_at' => $this->timestamp()->defaultValue('NOW()'),
            'deleted_at' => $this->timestamp(),
        ]);

        //These Dbs don't support foreign keys
        if(in_array(get_class($this->db->schema), [
          \yii\db\sqlite\Schema::class
        ])) {
          return;
        }

        $this->addForeignKey('fk_friend_user', '{{'.$table.'}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk_friend_friend', '{{'.$table.'}}', 'friend_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * {@inheritdoc}
     */
    public function safedown()
    {
        $table = \wukdo\models\Friend::tableName();
        $this->dropTable($table);
    }
}
