<?php

use yii\db\Migration;
use yii\helpers\ArrayHelper;

class m170219_203617_init_rbac extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        foreach (static::getPermissions() as $options) {
            $existing = $auth->getPermission($options['name']);
            if (!$existing) {
                $permission = $auth->createPermission($options['name']);
                $permission->description = $options['description'];
            } else {
                $permission = $existing;
            }
            $permissionClass = $options['rule'];
            $rule = new $permissionClass();
            $existing = $auth->getRule($rule->name);
            if (!$existing) {
                $auth->add($rule);
            } else {
                $rule = $existing;
            }
            $permission->ruleName = $rule->name;
            try {
                $auth->add($permission);
            } catch (\Exception $e) {}
        }

        //Attach child permissiona fter the fact
        foreach (static::getPermissions() as $options) {
            $permission = $auth->getPermission($options['name']);
            $children = ArrayHelper::getValue($options, 'children', []);
            foreach ($children as $child) {
                $child = $auth->getPermission($child);
                $auth->addChild($permission, $child);
            }
        }

        foreach (static::getRoles() as $options) {
            $existing = $auth->getRole($options['name']);
            if (!$existing) {
                $role = $auth->createRole($options['name']);
                $rule = $auth->getRule($options['rule']);
                if ($rule) {
                    $role->ruleName = $rule->name;
                }
                $auth->add($role);
            } else {
                $role = $existing;
            }
            foreach ($options['permissions'] as $permission) {
                $permission = $auth->getPermission($permission);
                $auth->addChild($role, $permission);
            }
        }
    }

    public function safeDown()
    {
        $auth = Yii::$app->authManager;
        foreach (static::getPermissions() as $permission) {
            $rule = new $permissionClass();
            $rule = $auth->getRule($rule->name);
            if ($rule) {
                $auth->remove($rule);
            }
            $permissionObject = $auth->getPermission($permission['name']);
            if ($permissionObject) {
                $auth->remove($permissionObject);
            }
        }

        foreach (static::getRoles() as $role) {
            $role = $auth->getRole($role['name']);
            if ($role) {
                $auth->remove($role);
            }
        }
    }

    protected static function getPermissions()
    {
        return  [
            [
                'name' => 'create',
                'description' => 'Create content',
                'rule' => \nitm\api\rbac\CanCreateRule::class,
            ], [
                'name' => 'update',
                'description' => 'Update content',
                'rule' => \nitm\api\rbac\CanUpdateRule::class,
            ], [
                'name' => 'delete',
                'description' => 'Delete content',
                'rule' => \nitm\api\rbac\CanDeleteRule::class,
            ], [
                'name' => 'view',
                'description' => 'View content',
                'rule' => \nitm\api\rbac\CanViewRule::class,
            ], [
                'name' => 'beUser',
                'description' => 'User rule',
                'rule' => \nitm\api\rbac\IsUserRule::class,
                'children' => ['create', 'update', 'delete', 'view'],
            ], [
                'name' => 'beOwner',
                'description' => 'Owner rule',
                'rule' => \nitm\api\rbac\IsOwnerRule::class,
                'children' => ['beUser', 'create', 'update', 'delete', 'view'],
            ], [
                'name' => 'beAdmin',
                'description' => 'Admin rule',
                'rule' => \nitm\api\rbac\IsAdminRule::class,
                'children' => ['beOwner', 'beUser', 'create', 'update', 'delete', 'view'],
            ],
        ];
    }

    protected static function getRoles()
    {
        return [
            [
                'name' => 'User',
                'permissions' => ['beOwner', 'beUser', 'create', 'update', 'delete', 'view'],
                'rule' => 'IsUserRule',
            ], [
                'name' => 'Admin',
                'permissions' => ['beOwner', 'beAdmin', 'create', 'update', 'delete', 'view'],
                'rule' => 'IsAdminRule',
            ],
        ];
    }
}
