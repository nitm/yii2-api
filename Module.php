<?php

namespace nitm\api;

use nitm\helpers\Response;
use nitm\helpers\ArrayHelper;
use yii\base\Event;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use nitm\api\filters\auth\QueryParamAuth;
use dektrium\user\Finder;

class Module extends \nitm\Module implements \yii\base\BootstrapInterface
{
    public $id = 'nitm-api';
    public $controllerNamespace = 'nitm\api\controllers';
    public $configModelNamespace = 'app\models';

    /**
     * Enable config functionality?
     *
     * @var bool
     */
    public $enableConfig = true;

    /**
     * Enable auth functionality?
     *
     * @var bool
     */
    public $enableAuth = true;

    /**
     * Enable favoriting functionality?
     *
     * @var bool
     */
    public $enableFavorites = true;

    /**
     * Enable liking functionality?
     *
     * @var bool
     */
    public $enableLikes = true;

    /**
     * Enable following functionality?
     *
     * @var bool
     */
    public $enableFollows = true;

    /**
     * Enable friends functionality?
     *
     * @var bool
     */
    public $enableFriends = true;

    /**
     * Enable profile functionality?
     *
     * @var bool
     */
    public $enableProfile = true;

    /**
     * Enable profile functionality?
     *
     * @var bool
     */
    public $enableFeed = true;

    /**
     * The default API version.
     *
     * @var string
     */
    public $version = null;

    /**
     * Should social auth be enabled? Enabled by default.
     *
     * @var bool
     */
    public $enableSocialAuth = false;

    public $tokenExpiry = 3600;
    public $eventMap = [];

    /**
     * Negotiate the response format for some pages.
     *
     * @var [type]
     * [
     *      html => [
     *          modules => [
     *             ...,
     *          ]
     *      ],
     *      ...
     * ]
     */
    public $responseOptions = [];

    protected $_settings = [];
    protected $_optionalAuth = [];
    protected $_corsHeaders;
    protected $_modelMap = [
        'User' => \nitm\api\models\User::class,
        'Profile' => \nitm\api\models\Profile::class,
        'Token' => \nitm\api\models\DektriumToken::class
    ];

    protected $_socialAuthOptions = [];

    /**
     * The response format for the API
     * @var string
     */
    protected $_responseFormat = 'json';

    protected $_supportedFormats = ['json', 'xml', 'html'];

    public function behaviors()
    {
        $behaviors = [
            'rateLimiter' => [
                'class' => \yii\filters\RateLimiter::class,
            ],
            'cors' => [
                'class' => \yii\filters\Cors::class,
                'cors' => $this->corsHeaders,
            ],
            'contentNegotiator' => [
                'class' => \yii\filters\ContentNegotiator::class,
                'formats' => [
                    'application/vnd.api+json' => 'json',
                    'application/json' => 'json',
                    'application/html' => 'html',
                ],
            ],
            'authenticator' => [
                'class' => CompositeAuth::class,
                'authMethods' => [
                    [
                        'class' => QueryParamAuth::class,
                        'except' => ['options'],
                    ], [
                        'class' => HttpBearerAuth::class,
                        'except' => ['options'],
                    ],
                ],
                'optional' => $this->_optionalAuth,
                'except' => ['options'],
            ],
        ];

        return array_replace_recursive(parent::behaviors(), $behaviors);
    }

    public function bootstrap($app)
    {
        $this->initUserEvents();
        $this->initUserComponent();
        $this->initModelMap();
        $this->initResponseHandler();
        $this->initUrlRules();
        $this->initMigrations([
            '@nitm/yii2-api/migrations',
        ]);
        $this->initViews([
            '@app/views/page-config-admin' => '@nitm/api/views/page-config-admin'
        ]);
        if ($this->enableSocialAuth) {
            $this->initSocialAuthComponent();
        }

        $this->initGiiGenerators([
             'action' => [
                'class' => 'nitm\api\generators\action\Generator', // generator class
                'templates' => [ //setting for out templates
                    'apiAction' => \Yii::getAlias('@nitm/api/generators/action/default'), // template name => path to template
                ]
            ],
             'controller' => [
                'class' => 'nitm\api\generators\controller\Generator', // generator class
                'templates' => [ //setting for out templates
                    'apiController' => \Yii::getAlias('@nitm/api/generators/controller/default'), // template name => path to template
                ]
            ],
            'model' => [
                'templates' => [ //setting for out templates
                    'apiModel' => \Yii::getAlias('@nitm/api/generators/model/default'), // template name => path to template
                ]
            ]
        ]);
    }

    public function beforeAction($action)
    {
        /*
         * For some retarded reason yii2 Cors is not setting certain headers. Setting them manually.
         */
        \yii\filters\Cors::addCorsHeaders(\Yii::$app->getResponse(), $this->corsHeaders);
        //Some support for OPTIONS requests
        switch (\Yii::$app->request->method) {
            case 'OPTIONS':
            case 'HEAD':
            return false;
            break;
        }

        return parent::beforeAction($action);
    }

    public function afterAction($action, $result)
    {
        $this->log([
            'message' => 'Api was accessed for ['.get_class($action->controller).'::'.get_class($action).'::'.$action->id.'::'.ArrayHelper::getValue($action, 'controller.modelClass').']',
            'action' => $action->id,
            'table_name' => property_exists($action->controller, 'model') && $action->controller->model ? $action->controller->model->is : $action->controller->id
        ], 'info', $this->id, ArrayHelper::getValue(\Yii::$app->controller, 'model'));
        return $action->controller->afterAction($action, $result);
    }

    public function getModelClass($for)
    {
        $class = ArrayHelper::getValue($this->modelMap, $for, null);
        if (!class_exists($class)) {
            throw new \yii\base\UnknownClassException("Unable to find class: $class");
        }

        return $class;
    }

    public function setSettings($settings)
    {
        $this->_settings = $settings;
    }

    public function setOptionalAuth($options = [])
    {
        $this->_optionalAuth = array_merge($this->_optionalAuth, [
            'security/login', 'security/auth', 'security/social',
            'recovery/reset', 'recovery/request',
            'index/index', 'config/view'
        ], $options);
    }

    public function setResponseFormat($format)
    {
        $format = strtolower($format);
        if (!in_array($format, $this->_supportedFormats)) {
            \Yii::warning("Requested format: $format. Only formats supported are [".implode(',', $this->_supportedFormats)."]. Defaulting to json format");
            $format = 'json';
        }
        $this->_responseFormat = $format;
    }

    public function setSocialAuthOptions(array $options)
    {
        $this->_socialAuthOptions = array_replace_recursive([
           'clients' => [],
           'routesOn' => [
             'connect' => 'registration/connect',
             'login' => 'user/login',
             'auth' => 'security/auth',
             'return' => 'register/connect',
           ],
           'urls' => [
               'website' => null,
               'api' => null,
            ],
      ], $options);
    }

    public function getSocialAuthOptions()
    {
        return $this->_socialAuthOptions;
    }

    public function getCorsHeaders()
    {
        if (!$this->_corsHeaders) {
            $this->_corsHeaders = $this->defaultCorsHeaders;
        }

        return $this->_corsHeaders;
    }

    public function setCorsHeaders(array $headers)
    {
        $this->_corsHeaders = array_replace_recursive($this->corsHeaders, $headers);
    }

    public function getModelMap()
    {
        return $this->_modelMap;
    }

    public function setModelMap(array $map)
    {
        $this->_modelMap = array_replace_recursive($this->modelMap, $map);
    }

    /**
     * We need tomerge some module specific functionality for social authentication.
     *
     * @method initSocialAuthComponent
     *
     * @return [type] [description]
     */
    protected function initSocialAuthComponent()
    {
        $authClientCollection = \Yii::$app->get('authClientCollection');
        if (!$authClientCollection) {
            \Yii::$app->authClientCollection = new \yii\authclient\Collection();
        }
        $authClientCollection->setClients($this->socialAuthClients);
    }

    protected function initUserComponent()
    {
        $userClass = $this->modelMap['User'];
        $user = \Yii::$app->user;
        $user->enableAutoLogin = false;
        $user->enableSession = false;
        $user->loginUrl = null;
        $user->identityClass = $this->modelMap['User'];

        /*
         * We need to change the dektrium user model class used for querying users.
         *
         * @var [type]
         */
        $container = \Yii::$container;
        $finder = $container->get(Finder::className());
        $finder->userQuery = $userClass::find();
        $container->set('dektrium\user\models\User', $userClass);

        $profileClass = $this->modelMap['Profile'];
        $finder->profileQuery = $profileClass::find();
        $container->set('dektrium\user\models\Profile', $profileClass);

        $profileClass = $this->modelMap['Token'];
        $finder->profileQuery = $profileClass::find();
        $container->set('dektrium\user\models\Token', $profileClass);
    }

    public function getUrls($id = 'admin')
    {
        $parameters = $routes = [];
        $routeHelper = new \nitm\helpers\Routes([
            'globalOnly' => true,
            'moduleId' => $this->id,
            'version' => $this->version,
        ]);
        if ($this->enableAuth) {
            $map = [
                'none' => '<controller:%controllers%>',
                'action-only' => '<controller:%controllers%>/<action>',
                'action-id' => '<controller:%controllers%>/<action>/<id>',
                'action-type-id' => '<controller:%controllers%>/<action>/<type>/<id>',
                'code' => '<controller:%controllers%>/<action>/<code>',
                'id-code' => '<controller:%controllers%>/<action>/<id>/<code>',
                'token' => '<controller:%controllers%>/<action>/<id>/<token>',
                'forgot' => 'recovery/request',
                'docs' => ['docs' => 'index'],
            ];

            $parameters = [
                'code' => ['registration', 'security'],
                'id-code' => ['recovery'],
                'token' => ['registration'],
                'action-only' => ['security', 'registration', 'recovery', 'site', 'page-config-admin'],
                'action-id' => ['page-config-admin'],
                'action-type-id' => ['page-config-admin'],
                'docs' => ['index', 'docs'],
            ];

            //Create module routes
            $routeHelper->map = $map;
            $routeHelper->controllers = [
                'security', 'registration', 'recovery', 'site', 'profile', 'page-config-admin'
            ];

            $routes = $routeHelper->create($parameters);
        }

        //Create versioned module routes
        $routeHelper->clear();
        $routeHelper->version = '<version>';
      //   $routeHelper->routePrefix = 'version';
        $routeHelper->addRules(null, $map);

        $routes = array_merge($routes, $routeHelper->create($parameters));

        if ($this->enableConfig) {
            $routes[] = [
                'class' => 'yii\rest\UrlRule',
                'controller' => [
                    'config' => 'rest-api/config',
                ],
                'pluralize' => true,
                'except' => ['update', 'create', 'delete', 'index'],
                'tokens' => [
                    '{id}' => '<id>',
                ],
            ];
        }

        if ($this->enableFeed) {
            $routes[] = [
                'class' => 'yii\rest\UrlRule',
                'controller' => [
                    'feed' => 'rest-api/feed',
                    'activity' => 'rest-api/feed',
                ],
                'pluralize' => true,
                'except' => ['update', 'create', 'delete'],
                'tokens' => [
                    '{id}' => '<id>',
                ],
            ];
        }

        if ($this->enableProfile) {
            $routes[] = [
                'class' => 'yii\rest\UrlRule',
                'controller' => [
                    'profile' => 'rest-api/profile',
                ],
                'pluralize' => true,
                'except' => ['create', 'delete'],
                'tokens' => [
                    '{id}' => '<id>',
                ],
                'extraPatterns' => [
                    'POST {id}' => 'update',
                    'POST' => 'update',
                ],
            ];

            $routes[] = [
                'class' => 'yii\rest\UrlRule',
                'controller' => [
                    'user' => 'rest-api/member',
                ],
                'pluralize' => true,
                'except' => ['create', 'delete'],
                'tokens' => [
                    '{id}' => '<id>',
                ],
            ];
        }

        if ($this->enableLikes
            || $this->enableFavorites
            || $this->enableFriends
            || $this->enableFollows) {
            $routes['enabled'] = [
              'class' => 'yii\rest\UrlRule',
              'controller' => [
              ],
              'except' => ['update'],
              'tokens' => [
                  '{id}' => '<id>',
                  '{type}' => '<type>',
              ],
              'patterns' => [
                  'POST {type}/{id}' => 'create',
                  'POST /' => 'create',
                  'DELETE {type}/{id}' => 'delete',
                  'GET,HEAD {type}/{id}' => 'index',
                  'GET,HEAD ' => 'index',
                  'GET,HEAD {id}' => 'index',
                  '{id}' => 'options',
                  '{type}/{id}' => 'options',
                  '' => 'options',
              ],
          ];


            if ($this->enableLikes) {
                $routes['enabled']['controller'] = array_merge($routes['enabled']['controller'], [
                  'likes' => 'rest-api/likes',
                  'like' => 'rest-api/likes',
                  'dislikes' => 'rest-api/dislikes',
                  'dislike' => 'rest-api/dislikes',
              ]);
            }

            if ($this->enableFavorites) {
                $routes['enabled']['controller'] = array_merge($routes['enabled']['controller'], [
                  'favorite' => 'rest-api/favorites',
                  'favorites' => 'rest-api/favorites',
                  'favorites-list' => 'rest-api/favorites',
              ]);
            }

            if ($this->enableFollows) {
                $routes['enabled']['controller'] = array_merge($routes['enabled']['controller'], [
                  'follow' => 'rest-api/follow',
                  'follows' => 'rest-api/follow',
              ]);
            }

            if ($this->enableFriends) {
                $routes['enabled']['controller'] = array_merge($routes['enabled']['controller'], [
                  'friends-list' => 'rest-api/friend',
              ]);
            }
        }

        return array_values($routes);
    }

    protected function initResponseHandler()
    {
      // We ignore this for the console response
      if(\Yii::$app->response instanceof \yii\console\Response) {
        return;
      }
        $this->responseOptions = array_merge($this->defaultResponseOptions, $this->responseOptions);
        $response = \Yii::$app->response;
        $response->format = Response::getFormat();
        $response->on('beforeSend', function ($event) {
            if (Response::formatSpecified()
                || (Response::isFormatSpecifiedExplicitly()
                    && !in_array(Response::getFormat(), $this->getBehavior('contentNegotiator')->formats))
                ) {
                return;
            }

            $response = $event->sender;

            if (!$response->isSuccessful && is_string($response->data)) {
                $response->format = 'html';
                return;
            }
            $response->format = $this->_responseFormat;
            if (\Yii::$app->requestedAction && in_array($response->statusCode, [
                200, 302, 301
            ])) {
                $controllerId = \Yii::$app->requestedAction->controller->id;
                $actionId = \Yii::$app->requestedAction->id;
                $htmlOptions = ArrayHelper::getValue($this->responseOptions, 'html');
                $moduleClass = \Yii::$app->requestedAction->controller->module->className();
                $options = ArrayHelper::getValue($htmlOptions, $moduleClass, false);
                //If the current module needs to be formatted in HTML
                if ($options !== false) {
                    //If there are controller based limitations
                    if (is_array($options)) {
                        //If there are id based limitations
                        if (ArrayHelper::isAssociative($options)) {
                            $actionOptions = ArrayHelper::getValue($options, $controllerId, null);
                            if ($actionOptions && in_array($actionId, $actionOptions)) {
                                $response->format = 'html';

                                return;
                            }
                        } elseif (in_array($controllerId, $options)) {
                            $response->format = 'html';

                            return;
                        }
                    } else {
                        $response->format = 'html';

                        return;
                    }
                }
            }
            $response->data = [
                'success' => $response->isSuccessful,
                'data' => $response->data,
            ];
        });
    }

    protected function initModelMap()
    {
        $this->modelMap = array_merge($this->defaultModelMap, $this->modelMap);
        \Yii::$app->getModule('user')->modelMap['User'] = $this->modelMap['User'];
        \Yii::$app->getModule('user')->modelMap['Profile'] = $this->modelMap['Profile'];
    }

    /**
     * Child classes should implement this function.
     *
     * @method initUserEvents
     *
     * @return [type] [description]
     */
    protected function initUserEvents()
    {
        //Events for after the user has logged in
        $loginEvents = ArrayHelper::getValue($this->eventMap, 'login');
        if (!empty($loginEvents)) {
            foreach ($loginEvents as $class => $event) {
                Event::on($class, $event, function ($event) {
                    $user = \Yii::$app->user->identity;
                    if (!$user->apiToken) {
                        $user->generateApiToken();
                    }
                //Add token to the headers
                $headers = \Yii::$app->response->headers;
                    $headers->set('Access-Token', $user->apiToken->token);
                });
            }
        }
    }

    protected function getDefaultCorsHeaders()
    {
        $serverName = defined('YII_ENV') && YII_ENV != 'production' ? '*' : $_SERVER['SERVER_NAME'];

        return [
           'Origin' => [
               $serverName,
            ],
           'Access-Control-Allow-Credentials' => 'true',
           'Access-Control-Allow-Origin' => [$serverName],
           'Access-Control-Expose-Headers' => [
               'X-Csrf-Token', 'Access-Token',
            ],
           'Access-Control-Allow-Headers' => [
               'X-Csrf-Token', 'X-Requested-With', 'Content-Type', 'Authorization',
            ],
           'Access-Control-Allow-Methods' => [
               'GET', 'POST', 'HEAD', 'OPTIONS', 'PUT', 'PATCH',
            ],
       ];
    }

    protected function getDefaultModelMap()
    {
        return [
            'LoginForm' => \nitm\api\models\LoginForm::class,
            'RegistrationForm' => \nitm\api\models\RegistrationForm::class,
            'ResendForm' => \nitm\api\models\ResendForm::class,
            'RecoveryForm' => \nitm\api\models\RecoveryForm::class,
            'User' => \nitm\api\models\User::class,
            'Profile' => \nitm\api\models\Profile::class,
        ];
    }

    protected function getDefaultResponseOptions()
    {
        return [
            'html' => [
                \yii\debug\Module::className() => true,
                \yii\gii\Module::className() => true,
                \dektrium\rbac\RbacWebModule::className() => true,
                \dektrium\user\Module::className() => [
                    'admin', 'user',
                ],
                \yii\web\Applicaiton::class => [
                    'index' => ['doc', 'index', 'error'],
                ],
                static::className() => [
                   'security' => ['connect', 'auth'],
                   'recovery' => ['reset'],
                   'page-config-admin', [
                       'index', 'create', 'update', 'view', 'delete'
                    ],
                ],
            ],
        ];
    }

    protected function getSocialAuthClients()
    {
        return array_replace_recursive(array_intersect_key([
              'facebook' => [
                  'class' => 'dektrium\user\clients\Facebook',
                  'returnUrl' => \nitm\api\helpers\Auth::getAuthReferer(null, 'api'),
              ],
              'google' => [
                  'class' => 'dektrium\user\clients\Google',
                  'returnUrl' => \nitm\api\helpers\Auth::getAuthReferer(null, 'api'),
              ],
              'twitter' => [
                  'class' => 'dektrium\user\clients\Twitter',
                  'returnUrl' => \nitm\api\helpers\Auth::getAuthReferer(null, 'api'),
              ],
          ], $this->socialAuthOptions['clients']), $this->socialAuthOptions['clients']);
    }
}
