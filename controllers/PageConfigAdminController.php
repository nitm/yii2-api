<?php

namespace nitm\api\controllers;

use Yii;
use nitm\api\models\PageConfig;
use yii\data\ActiveDataProvider;
use nitm\controllers\DefaultController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\ArrayHelper;

/**
 * PageConfigAdminController implements the CRUD actions for PageConfig model.
 */
class PageConfigAdminController extends DefaultController
{
    use \nitm\api\traits\Controller;

    public $modelClass = '\nitm\api\models\PageConfig';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        return array_replace_recursive(parent::behaviors(), [
            'access' => [
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['get-field'],
                        'roles' => ['@'],
                    ],
                ]
            ],
            'config' => [
                'class' => \nitm\behaviors\Settings::class
            ]
        ]);
    }

    // public function actionUpdate($id, $modelClass=null, $with=[], $options=[]) {
    //     print_r($_POST);
    //     exit;
    // }

    public function actionGetField()
    {
        $this->responseFormat = 'json';
        $data = $this->resolveData();
        $id = ArrayHelper::getValue(\Yii::$app->request->get(), 'id');
        if (empty($data) || !$id) {
            return false;
        }
        $this->model = $this->findModel($id);
        $this->model->configType = $data['configType'];
        $this->model->configKey = $data['configKey'];
        $this->model->configClass = ArrayHelper::getValue($data, 'configClass');
        $this->model->configOptions = ArrayHelper::getValue($data, 'configOptions');
        $this->model->id = $this->model->configKey;
        if(array_key_exists($this->model->section($this->model->configKey), $this->model->config)) {
            $html = $this->renderInput();
            $parent = '#'.$this->model->htmlParentId($this->model->configKey);
        } else {
            $html = $this->renderList();
            $parent = '#'.$this->model->htmlParentId('');
        }
        $ret_val = [
            'dataHtml' => $html,
            'id' => $this->model->configKey,
            'parent' => $parent
        ];
        return [true, $ret_val];
    }

    protected function renderList() {
        return $this->renderAjax('list', [
            'model' => $this->model,
            'listId' => $this->model->htmlId($this->model->configKey).'-children',
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => [
                    $this->model->section($this->model->configKey) => [
                        [
                            'id' => $this->model->htmlId($this->model->configKey),
                            'key' => $this->model->configKey,
                            'type' => $this->model->configType,
                            'class' => $this->model->configClass,
                            'options' => $this->model->configOptions,
                            'section' => $this->model->section($this->model->configKey),
                            'value' => null,
                            'title' => $this->model->realKey($this->model->configKey),
                        ]
                    ]
                ]
            ])
        ]);
    }

    protected function renderInput() {
        return $this->renderAjax('_form-input', [
            'id' => $this->model->htmlId($this->model->configKey),
            'key' => $this->model->configKey,
            'type' => $this->model->configType,
            'class' => $this->model->configClass,
            'options' => $this->model->configOptions,
            'section' => $this->model->section($this->model->configKey),
            'value' => null,
            'title' => $this->model->realKey($this->model->configKey),
            'children' => $this->renderAjax('list', [
                'model' => $this->model,
                'listId' => $this->model->htmlId($this->model->configKey).'-children',
                'dataProvider' => new \yii\data\ArrayDataProvider(['allModels' => []])
            ])
        ]);
    }

    /**
     * Helper function to get the title and delete button for each conig entry
     * @param  AppConfig $model The app config model
     * @return string        The HTML button
     */
    public function getTitleAndDeleteButton($model, $viewId)
    {
        $deleteButton = Html::button('Delete', [
            'class' => 'btn btn-danger',
            'data-confirm' => "Are you sure you want to delete the ".$model->page." value?",
            "data-method" => "post",
            "title" => "Delete ".$model->title(),
            'role' => 'removeParent',
            'data-parent' => '[role="itemWrapper"]'
        ]);
        $viewButton = Html::button('Update', [
             'class' => 'btn btn-info',
             "title" => "Update ".$model->title(),
             'onclick' => '$("'.$viewId.'").slideToggle()'
         ]);
        return Html::tag('h2', $model->title()." [".$model->configKey."] ".$viewButton.' '.$deleteButton);
    }
}
