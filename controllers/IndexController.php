<?php

namespace nitm\api\controllers;

use nitm\helpers\Response;
use nitm\helpers\ArrayHelper;

class IndexController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $this->redirect('/documentation/html/guide-README.html');
    }
}
