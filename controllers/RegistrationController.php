<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace nitm\api\controllers;

use dektrium\user\Finder;
use nitm\api\models\RegistrationForm;
use dektrium\user\models\ResendForm;
use dektrium\user\models\User;
use dektrium\user\models\Account;
use Yii;
use yii\web\NotFoundHttpException;
use nitm\helpers\ArrayHelper;
use nitm\api\helpers\Auth as AuthHelper;

/**
 * RegistrationController is responsible for all registration process, which includes registration of a new account,
 * resending confirmation tokens, email confirmation and registration via social networks.
 *
 * @property \dektrium\user\Module $module
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class RegistrationController extends \dektrium\user\controllers\RegistrationController
{
    use \nitm\traits\Controller,
      \nitm\api\traits\Controller,
      \nitm\api\traits\UserController,
      \nitm\traits\EventTraits,
      \nitm\api\traits\SocialConnect;

    public function init()
    {
        parent::init();
        $this->module = \Yii::$app->getModule('user');
        $this->attachToThis([
            self::EVENT_AFTER_CONNECT => [$this, 'afterConnect'],
            self::EVENT_AFTER_CONFIRM => [$this, 'afterConfirm'],
            self::EVENT_AFTER_RESEND => [$this, 'afterResend'],
            self::EVENT_AFTER_REGISTER => [$this, 'afterRegister'],
        ]);
    }

    /** {@inheritdoc} */
    public function behaviors()
    {
        if (\Yii::$app->getRequest()->isAjax) {
            $this->setResponseFormat('json');
        }

        return array_merge_recursive(parent::behaviors(), $this->defaultBehaviors());
    }

    /**
     * Performs registration.
     *
     * @return json
     *
     * @throws \yii\web\HttpException
     */
    public function actionRegister()
    {
        if (!\Yii::$app->getModule('user')->enableRegistration) {
            throw new NotFoundHttpException();
        }

        $ret_val = [
            'success' => false,
        ];
        /** @var RegistrationForm $model */
        $class = $this->apiModule->getModelClass('RegistrationForm');
        $model = Yii::createObject($class::className());
        $event = $this->getFormEvent($model);

        $this->trigger(self::EVENT_BEFORE_REGISTER, $event);

        $model->load([$class::formName() => $this->resolveData()]);

        if (!$model->register()) {
            $ret_val['message'] = 'Damnit'.($model->username ? ' '.$model->username : '').', there was a problem...check the error messages';
            foreach ($model->getErrors() as $attribute => $error) {
                $ret_val['error'][\yii\helpers\Inflector::variablize($attribute)] = implode(', ', $error);
            }

            throw new \yii\web\BadRequestHttpException(implode('. ', $ret_val['error']));
        } else {
            $this->trigger(self::EVENT_AFTER_REGISTER, $event);
            $ret_val['success'] = true;
            $ret_val['message'] = 'Welcome to '.\Yii::$app->name.' '.$model->username.'!';
            $ret_val['user'] = $model->user;
        }
        $user = ArrayHelper::remove($ret_val, 'user', []);

        return ['register' => $ret_val, 'user' => $user];
    }

    /**
     * Displays page where user can create new account that will be connected to social account.
     *
     * @param string $code
     *
     * @return string
     *
     * @throws NotFoundHttpException
     */
    public function actionConnect($code)
    {
        $account = $this->finder->findAccount()->byCode($code)->one();
        return $this->connectAccount($code, $account);
    }

    /**
     * Confirms user's account. If confirmation was successful logs the user and shows success message. Otherwise
     * shows error message.
     *
     * @param int    $id
     * @param string $code
     *
     * @return string
     *
     * @throws \yii\web\HttpException
     */
    public function actionConfirm($id, $code)
    {
        $user = $this->finder->findUserById($id);

        if ($user === null || $this->module->enableConfirmation == false) {
            throw new NotFoundHttpException();
        }

        $event = $this->getUserEvent($user);

        $this->trigger(self::EVENT_BEFORE_CONFIRM, $event);

        $user->attemptConfirmation($code);

        $this->trigger(self::EVENT_AFTER_CONFIRM, $event);

        return [
            'confirm' => [
                'isConfirmed' => $user->getIsConfirmed(),
            ],
        ];
    }

    /**
     * Displays page where user can request new confirmation token. If resending was successful, displays message.
     *
     * @return string
     *
     * @throws \yii\web\HttpException
     */
    public function actionResend()
    {
        if ($this->module->enableConfirmation == false) {
            throw new NotFoundHttpException();
        }

            /** @var ResendForm $model */
            $class = $this->apiModule->getModelClass('ResendForm');
        $model = Yii::createObject($class::className());

        $event = $this->getFormEvent($model);

        $this->trigger(self::EVENT_BEFORE_RESEND, $event);

        $this->performAjaxValidation($model);

        if ($model->load($this->resolveData()) && $model->resend()) {
            $this->trigger(self::EVENT_AFTER_RESEND, $event);

            return [
                'resend' => [
                    'message' => Yii::t('user', 'A new confirmation link has been sent'),
                ],
            ];
        }

        return [];
    }

    public function afterRegister($event)
    {
        $this->appendAccessToken();
    }

    public function afterConnect($event)
    {
        $this->appendAccessToken();
    }
}
