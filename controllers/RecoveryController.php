<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace nitm\api\controllers;

use dektrium\user\Finder;
use dektrium\user\models\RecoveryForm;
use dektrium\user\models\Token;
use dektrium\user\traits\AjaxValidationTrait;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use nitm\helpers\ArrayHelper;

/**
 * RecoveryController manages password recovery process.
 *
 * @property \dektrium\user\Module $module
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class RecoveryController extends \dektrium\user\controllers\RecoveryController
{
    use \nitm\traits\Controller, \nitm\api\traits\Controller, \nitm\api\traits\UserController;

    /** @inheritdoc */
    public function behaviors()
    {
        $behaviors = [
        ];
        $behaviors = array_merge_recursive(parent::behaviors(), $this->defaultBehaviors(), $behaviors);
        return $behaviors;
    }

    /**
     * Shows page where user can request password recovery.
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionRequest()
    {
        if (\Yii::$app->request->method == 'OPTIONS') {
            return true;
        }

        if (!\Yii::$app->getModule('user')->enablePasswordRecovery) {
            throw new NotFoundHttpException();
        }

        $ret_val = [];

        /** @var RecoveryForm $model */
        $class = $this->apiModule->getModelClass('RecoveryForm');
        $model = \Yii::createObject([
            'class'    => $class::className(),
            'scenario' => $class::SCENARIO_REQUEST,
        ]);

        $event = $this->getFormEvent($model);
        $this->performAjaxValidation($model);
        $this->trigger(self::EVENT_BEFORE_REQUEST, $event);

        $post = [
            $class::formName() => $this->resolveData($class::formName(), $this->resolveData())
        ];
        $view = '@nitm/api/views/recovery/request';

        if ($model->load($post) && $model->sendRecoveryMessage()) {
            $ret_val = [
                'forgot' => [
                    'success' => true,
                    'message'  => Yii::t('user', 'Recovery message sent. Please check your email for instructions')
                ]
            ];
            if($this->responseFormat == 'html') {
                return $this->render($view, [
                    'model' => $model,
                    'result' => $ret_val
                ]);
            }
            return $ret_val;
        } else if($model->hasErrors()){
            $ret_val['error']['message'] = \nitm\helpers\Model::formatErrors($model) ?: Yii::t('user', 'Unable to send recovery message');
        }

        \Yii::$app->getResponse()->statusCode = 404;

        if($this->responseFormat == 'html') {
            return $this->render($view, [
                'model' => $model,
                'result' => $ret_val
            ]);
        }

        return $ret_val;
    }

    /**
     * Displays page where user can reset password.
     *
     * @param int    $id
     * @param string $code
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionReset($id, $code)
    {
        if (!\Yii::$app->getModule('user')->enablePasswordRecovery) {
            throw new NotFoundHttpException();
        }

        $ret_val = [
            'success' => false,
            'message' => Yii::t("user", "You have a valid token. Enter a new password to complete the reset process.")
        ];
        $class = $this->apiModule->getModelClass('RecoveryForm');

        /** @var Token $token */
        $token = $this->finder->findToken(['user_id' => $id, 'code' => $code, 'type' => Token::TYPE_RECOVERY])->one();
        $event = $this->getResetPasswordEvent($token);

        $this->trigger(self::EVENT_BEFORE_TOKEN_VALIDATE, $event);

        if ($token === null || $token->isExpired || $token->user === null) {
            $this->trigger(self::EVENT_AFTER_TOKEN_VALIDATE, $event);
            \Yii::$app->getResponse()->statusCode = 404;
            $ret_val = [
                'error' => [
                    'message' => Yii::t('user', 'Recovery link is invalid or expired. Please try requesting a new one.'),
                    'invalid' => true
                ]
            ];
        }

        /** @var RecoveryForm $model */
        $model = Yii::createObject([
            'class'    => $class::className(),
            'scenario' => $class::SCENARIO_RESET,
        ]);

        $this->performAjaxValidation($model);

        $post = $this->resolveData($class::formName(), $this->resolveData());
        
        $view = '@nitm/api/views/recovery/reset';
        if (!empty($post)) {
            $post = array_filter([
                $class::formName() => array_filter($post)
            ]);

            if ($model->load($post) && $token) {
                if ($model->resetPassword($token)) {
                    $ret_val = [
                      'resetPassword' => [
                            'message'  => Yii::t('user', 'Congrats! Your password has been changed. You can try logging in again'),
                            'success' => true
                        ]
                    ];
                } else {
                    $ret_val = [
                        'error' => [
                            'message' => Yii::t('user', 'An error occurred while resetting your password.'.\nitm\helpers\Model::formatErrors($model))
                        ]
                    ];
                }
            }
        }

        if($this->responseFormat == 'html') {
            return $this->render($view, [
                'model' => $model,
                'result' => $ret_val
            ]);
        }

        return $ret_val;
    }
}
