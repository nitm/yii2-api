<?php

namespace nitm\api\controllers;

use pickledup\models\User;

/**
 * UserController implements the CRUD actions for User model.
 */
class UsersController extends BaseActiveApiController
{
    public $modelClass = 'nitm\api\models\User';

    public function actions()
    {
        $actions = parent::actions();

        // disable the "delete" and "create" actions
        unset($actions['delete'], $actions['create']);

        // customize the data provider preparation with the "prepareDataProvider()" method
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        //Change the user view aciton to a custom one
        $actions['view']['class'] = \nitm\api\actions\view\UserAction::className();

        return $actions;
    }
}
