<?php

namespace nitm\api\controllers;

use nitm\api\models\Like;

/**
 * LikeController implements the CRUD actions for Like model.
 */
class LikeController extends BaseActionController
{
    public $modelClass = 'nitm\api\models\Like';

    public function actions()
    {
        $actions = parent::actions();
        //Change the location view aciton to a custom one
        $actions['view']['class'] = \nitm\api\actions\view\LikeAction::className();

        //Change the location view aciton to a custom one
        $actions['index']['class'] = \nitm\api\actions\index\LikeAction::className();

        //Change the location create aciton to a custom one
        $actions['create']['class'] = \nitm\api\actions\create\LikeAction::className();
        //Remove the actions below as we have custom functions for them
        unset($actions['delete']);

        return $actions;
    }
}
