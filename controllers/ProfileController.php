<?php

namespace nitm\api\controllers;

use nitm\api\models\User;

/**
 * UserController implements the CRUD actions for User model.
 */
class ProfileController extends BaseActiveApiController
{
    public $modelClass = 'nitm\api\models\Profile';

    public function actions()
    {
        $actions = parent::actions();

        // disable the "delete" and "create" actions
        unset($actions['delete'], $actions['create'], $actions['index']);

        //Change the user view action to a custom one
        $actions['view']['class'] = \nitm\api\actions\view\ProfileAction::className();

        //Change the user update action to a custom one
        $actions['update']['class'] = \nitm\api\actions\update\ProfileAction::className();

        return $actions;
    }
}
