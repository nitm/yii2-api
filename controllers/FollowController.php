<?php

namespace nitm\api\controllers;

class FollowController extends BaseActiveApiController
{
    public $modelClass = 'nitm\api\models\Follow';

    public function actions()
    {
        $actions = parent::actions();

        // disable the "delete" and "create" actions
        unset($actions['update']);

        //Change the user view action to a custom one
        $actions['view']['class'] = \nitm\api\actions\view\FollowAction::className();

        //Change the user index action to a custom one
        $actions['index']['class'] = \nitm\api\actions\index\FollowAction::className();

        //Change the user index action to a custom one
        $actions['create']['class'] = \nitm\api\actions\create\FollowAction::className();

        return $actions;
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        return true;
       // check if the user can access $action and $model
       // throw ForbiddenHttpException if access should be denied

       switch (1) {
           case \Yii::$app->getUser()->can('isOwner'):
           case \Yii::$app->getUser()->can('viewFollowsList'):
           return true;
           break;

           default:
           throw new \yii\web\ForbiddenHttpException('No Access');
           break;
       }
    }
}
