<?php

namespace nitm\api\controllers;

class ConfigController extends BaseActiveApiController
{
    public $modelClass = '\nitm\api\models\PageConfig';

    public function actions() {
        $actions = parent::actions();
        $actions['index'] = \nitm\api\actions\index\ConfigAction::class;
        $actions['view'] = \nitm\api\actions\view\ConfigAction::class;
        unset($actions['create'], $actions['delete']);

        return $actions;
    }
}
