<?php

namespace nitm\api\controllers;

/**
 * FavoriteController implements the CRUD actions for Favorite model.
 */
class FavoriteController extends BaseActionController
{
    public $modelClass = 'nitm\api\models\Favorite';

    public function actions()
    {
        $actions = parent::actions();

        //Change the location view aciton to a custom one
        $actions['view']['class'] = \nitm\api\actions\view\FavoriteAction::className();

        //Change the location view aciton to a custom one
        $actions['index']['class'] = \nitm\api\actions\index\FavoriteAction::className();

        //Change the location create aciton to a custom one
        $actions['create']['class'] = \nitm\api\actions\create\FavoriteAction::className();

        //Change the location create aciton to a custom one
        $actions['delete']['class'] = \nitm\api\actions\delete\FavoriteAction::className();

        //Remove the actions below as we have custom functions for them
        unset($actions['update']);

        return $actions;
    }
}
