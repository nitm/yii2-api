<?php

namespace nitm\api\controllers;

class FeedController extends \nitm\api\controllers\BaseActiveApiController
{
    public $modelClass = '\nitm\api\models\Activity';

    public function actions()
    {
        $actions = parent::actions();

        //Change the user view aciton to a custom one
        $actions['view']['class'] = \nitm\api\actions\view\FeedAction::className();
        $actions['index']['class'] = \nitm\api\actions\index\IndexAction::className();

        return $actions;
    }
}
