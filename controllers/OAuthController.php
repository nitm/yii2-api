<?php

/*
 * This file implements  an API based version of the Dektrium authentication process
 */

namespace app\controllers;

use dektrium\user\Finder;
use dektrium\user\models\Account;
use dektrium\user\models\User;
use dektrium\user\Module;
use dektrium\user\traits\AjaxValidationTrait;
use Yii;
use yii\authclient\AuthAction;
use yii\authclient\ClientInterface;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use nitm\helpers\ArrayHelper;
use wukdo\models\LoginForm;

/**
 * Controller that manages user oAuth process
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class OAuthController extends DefaultApiController
{
    use \nitm\traits\Controller, \app\traits\UserController;

    public function init()
    {
        parent::init();
        $this->setResponseFormat('html');
    }

    /** @inheritdoc */
    public function behaviors()
    {
        $behaviors = [
            'access' => [
                'rules' => [
                    [
                        //'ips' => [gethostbyname(\Yii::$app->params['website'])],
                        'allow' => true,
                        'actions' => ['authenticate'],
                        'roles' => ['@']
                    ],
                ],
            ],
            'verbs' => [
                'actions' => [
                    'authenticate' => ['get', 'options']
                ],
            ],
        ];
        $behaviors = array_merge_recursive(parent::behaviors(), $this->defaultBehaviors(), $behaviors);
        return $behaviors;
    }

    /**
     * Tries to authenticate user via social network. If user has already used
     * this network's account, he will be logged in. Otherwise, it will try
     * to create new user account.
     *
     * @param ClientInterface $client
     */
    public function actionAuthenticate($provider)
    {
        $this->setResponseFormat('html');
        $account = $this->finder->findAccount()->byClient($client)->one();

        if ($account === null) {
            $account = Account::create($client);
        }

        if ($account->user instanceof User) {
            if ($account->user->isBlocked) {
                Yii::$app->session->setFlash('danger', Yii::t('user', 'Your account has been blocked.'));
                $this->action->successUrl = \Yii::$app->getUser()->setReturnUrl(\wukdo\helpers\Auth::getLoginReferrer());
            } else {
                Yii::$app->user->login($account->user, \Yii::$app->getModule('user')->rememberFor);
                //$this->action->successUrl = Yii::$app->getUser()->getReturnUrl();
                $this->action->successUrl = \wukdo\helpers\Auth::getReturnUrl();
            }
        } else {
            $this->action->successUrl = $account->getConnectUrl().'&'.\wukdo\helpers\Auth::getArgs();
        }
    }
}
