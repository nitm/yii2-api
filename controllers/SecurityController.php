<?php

/*
 * This file implements  an API based version of the Dektrium authentication process
 */

namespace nitm\api\controllers;

use dektrium\user\Finder;
use dektrium\user\models\Account;
use dektrium\user\Module;
use Yii;
use yii\authclient\ClientInterface;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use nitm\helpers\ArrayHelper;
use nitm\helpers\RequestHelper;

/**
 * Controller that manages user authentication process.
 *
 * @property Module $module
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class SecurityController extends \dektrium\user\controllers\SecurityController
{
    use \nitm\traits\Controller, \nitm\api\traits\UserController, \nitm\api\traits\Controller, \nitm\api\traits\SocialConnect, \nitm\traits\EventTraits;

    /** {@inheritdoc} */
    public function behaviors()
    {
        $behaviors = [
            'verbs' => [
                'actions' => [
                    'logout' => ['post', 'options'],
                ],
            ],
            'access' => [
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['ping', 'login', 'auth', 'social'],
                        'roles' => ['@', '?'],
                    ],
                ],
            ],
        ];
        $behaviors = array_replace_recursive(parent::behaviors(), $this->defaultBehaviors(), $behaviors);

        return $behaviors;
    }

    public function init()
    {
        parent::init();

        $this->forceAjax = true;
        $this->module = \Yii::$app->getModule('user');
        $this->attachToThis([
            self::EVENT_AFTER_CONNECT => [$this, 'afterConnect']
        ]);
    }

    /**
     * Performs the login authentication.
     *
     * @return string|Response
     */
    public function actionLogin()
    {
        $ret_val = [];

        if (!Yii::$app->user->isGuest) {
            $this->updateLastActive();

            return $this->getUserInfo();
        }

        list($isLoggedIn, $model) = $this->login();

        if ($isLoggedIn) {
            return $this->getUserInfo();
        } else {
            $message = implode('. ', array_map(function ($value) {
                return implode('. ', $value);
            }, $model->getErrors()));

            throw new \yii\web\BadRequestHttpException($message);
        }
    }

    /**
     * THis is used to refresh the user session.
     *
     * @return bool for current session
     */
    public function actionPing()
    {
        $ret_val = ['user' => []];
        if (Yii::$app->user->isGuest) {
            //Currently only using COOKIE sesson. If this fails return false.
           if ($this->loginByToken()) {
               $this->updateLastActive();

               return $this->getUserInfo();
           }
        }
        if (!\Yii::$app->user->isGuest) {
            $this->appendAccessToken();

            return $this->getUserInfo();
        } else {
            throw new \yii\web\ForbiddenHttpException('You need to login');
        }
    }

    /**
     * Logs the user out and then returns true on logout.
     *
     * @return Response
     */
    public function actionLogout()
    {
        $user = Yii::$app->user->identity;
        if ($user) {
            $event = $this->getUserEvent($user);
            $this->trigger(self::EVENT_BEFORE_LOGOUT, $event);
        }
        Yii::$app->getUser()->logout();
        if ($user) {
            $this->trigger(self::EVENT_AFTER_LOGOUT, $event);
        }
        \Yii::trace(\Yii::$app->getResponse()->getHeaders());

        return true;
    }

    public function afterLogin($event)
    {
        $this->appendAccessToken();
    }

    /**
     * This action handles connection a social account that has already been connected on the clients side
     * @param  [type] $code [description]
     * @return [type]       [description]
     */
    public function actionSocial($token=null)
    {
        $token = $token ?: $this->resolveData('token', $this->resolveData('code'), $this->resolveData('access-token'));
        $provider = $this->resolveData('provider', 'facebook');
        /**
         * User attributes in the format:
         * [
         *   id: string|int
         *   ...
         * ]
         * @var [type]
         */
        $client = \Yii::$app->authClientCollection->getClient($provider);
        $client->id = $this->resolveData('id');
        $client->userAttributes = $this->resolveData('data');
        $client->accessToken = [
            'token' => $token
        ];
        $account = $this->finder->findAccount()->byClient($client)->one();

        if (!$account) {
            try {
                $this->authenticate($client, true);
                $account = $this->finder->findAccount()->byClient($client)->one();
                $account->username = $client->userAttributes['email'];
                $account->email = $client->userAttributes['email'];
                $account->save();
            } catch (\Exception $e) {
                \Yii::warning($e);
                throw new \yii\web\BadRequestHttpException("Couldn't connect social account: ".$e->getMessage());
            }
        }

        return $this->connectAccount(null, $account);
    }

    public function afterConnect($event)
    {
        $this->appendAccessToken();
    }

    /**
     * Tries to authenticate user via social network. If user has already used
     * this network's account, he will be logged in. Otherwise, it will try
     * to create new user account.
     *
     * @param ClientInterface $client
     */
    public function authenticate(ClientInterface $client, $isLocal=false)
    {
        $account = $this->finder->findAccount()->byClient($client)->one();

        if ($account === null) {
            /** @var Account $account */
            $accountObj = Yii::createObject(Account::className());
            $account = $accountObj::create($client);
        }

        $event = $this->getAuthEvent($account, $client);

        $this->trigger(self::EVENT_BEFORE_AUTHENTICATE, $event);

        if ($account->user instanceof User) {
            if ($account->user->isBlocked) {
                if ($isLocal) {
                    $this->action->successUrl = \Yii::$app->getUser()->setReturnUrl(\nitm\api\helpers\Auth::getLoginReferer());
                } else {
                    throw new \yii\web\BadRequestHttpException("Account is temporarily blocked");
                }
            } else {
                Yii::$app->user->login($account->user, \Yii::$app->getModule('user')->rememberFor);
                //$this->action->successUrl = Yii::$app->getUser()->getReturnUrl();
                if (!$isLocal) {
                    $this->action->successUrl = \nitm\api\helpers\Auth::getReturnUrl();
                }
            }
        } else {
            //This is in order to generate and update the account code for the unlinked account
            $url = $account->getConnectUrl();
            parse_str(parse_url($url, PHP_URL_QUERY), $params);
            \Yii::trace('Code was : '.$account->code);
            if (!$isLocal) {
                $this->action->successUrl = \nitm\api\helpers\Auth::getConnectUrl($params);
            }
        }

        $this->trigger(self::EVENT_AFTER_AUTHENTICATE, $event);
    }

    /**
     * Tries to connect social account to user.
     * We change the return to URL to match the right destination on the frontend.
     *
     * @param ClientInterface $client
     */
    public function connect(ClientInterface $client)
    {
        /** @var Account $account */
        $account = Yii::createObject(Account::className());
        $event = $this->getAuthEvent($account, $client);
        $this->trigger(self::EVENT_BEFORE_CONNECT, $event);
        $account->connectWithUser($client);
        $this->trigger(self::EVENT_AFTER_CONNECT, $event);

        $this->action->successUrl = \nitm\api\helpers\Auth::getReturnUrl();
    }

    /**
     * Login using the credentials provided, or use those form $_POST.
     *
     * @param array $user
     *
     * @return array
     */
    protected function login($user = null)
    {
        $ret_val = false;
        $user = is_null($user) ? \Yii::$app->getRequest()->post() : $user;
        $user = empty($user) ? json_decode(file_get_contents("php://input"), true) : $user;
        $class = $this->apiModule->getModelClass('LoginForm');
        $user = ArrayHelper::getValue($user, $class::formName(), $user);

        /** @var LoginForm $model */
        $model = Yii::createObject($class::className());
        $event = $this->getFormEvent($model);

        $this->performAjaxValidation($model);
        $this->trigger(self::EVENT_BEFORE_LOGIN, $event);
        $model->load($user, '');
        if ($model->login()) {
            $this->trigger(self::EVENT_AFTER_LOGIN, $event);
            $this->afterLogin($event);
            $ret_val = true;
        }

        return [$ret_val, $model];
    }
}
