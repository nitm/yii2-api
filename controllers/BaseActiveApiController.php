<?php

namespace nitm\api\controllers;

use yii\helpers\ArrayHelper;
use nitm\api\models\LoginForm;

class BaseActiveApiController extends \yii\rest\ActiveController
{
    use \nitm\traits\Controller;

    public $serializer = [
        'class' => '\nitm\api\Serializer',
    ];

    public function actions()
    {
        $actions = parent::actions();

        //Change the location view aciton to a custom one
        $actions['view']['class'] = \nitm\api\actions\View::className();

        //Change the location view aciton to a custom one
        $actions['index']['class'] = \nitm\api\actions\Index::className();

        //Change the location create aciton to a custom one
        $actions['create']['class'] = \nitm\api\actions\Create::className();

        //Change the location create aciton to a custom one
        $actions['delete']['class'] = \nitm\api\actions\Delete::className();

        //Change the location create aciton to a custom one
        $actions['update']['class'] = \nitm\api\actions\Update::className();

        return $actions;
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD', 'OPTIONS'],
            'view' => ['GET', 'HEAD', 'OPTIONS'],
            'create' => ['POST', 'OPTIONS'],
            'update' => ['PUT', 'PATCH', 'POST', 'OPTIONS'],
            'delete' => ['DELETE', 'OPTIONS'],
        ];
    }

    public function beforeAction($action)
    {
        LoginForm::updateLastActive();

        return parent::beforeAction($action);
    }

    /**
     * Lists all $this->modelClass models.
     *
     * @return mixed
     */
    public function prepareDataProvider()
    {
        $this->setResponseFormat('json');
        $model = \Yii::createObject($this->modelClass);

        return $model->search();
    }

    /**
     * Checks the privilege of the current user.
     *
     * This method should be overridden to check whether the current user has the privilege
     * to run the specified action against the specified data model.
     * If the user does not have access, a [[ForbiddenHttpException]] should be thrown.
     *
     * @param string          $action the ID of the action to be executed
     * @param \yii\base\Model $model  the model to be accessed. If null, it means no specific model is being accessed
     * @param array           $params additional parameters
     *
     * @throws ForbiddenHttpException if the user does not have access
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        $requiresAuth = ['create', 'update', 'delete'];
        $this->checkUser(in_array($action, $requiresAuth));
        // check if the user can access $action and $model
        // throw ForbiddenHttpException if access should be denied
        if (\Yii::$app->user->isGuest && !\Yii::$app->session->get('tried-login')) {
            try {
                $this->loginByToken();
            } catch (\Exception $e) {
                \Yii::$app->session->set('tried-login', true);
            }
        }
        \Yii::$app->session->remove('tried-login');

        if ($model && in_array($action, $requiresAuth)) {
            if (\Yii::$app->user->can($action, ['model' => $model])) {
                $modelName = is_object($model) ? $model->isWhat(true) : '';
                throw new \yii\web\ForbiddenHttpException(sprintf("You don't have access to $action ".$modelName, $action));
            }
        }

        return true;
    }

    public function extractEmberPayload($args, $mustHave = [])
    {
        $extract = false;
        foreach ($mustHave as $key) {
            if (!isset($args[$key]) || empty($args['key'])) {
                $extract = true;
                break;
            }
        }

        if ($extract === true) {
            //Ember sends a json string in request. Using parsed rawBody data to get into
            $params = \Yii::$app->request->getBodyParams();
            if (is_array($params) && $params != []) {
                $params = array_shift($params);
                foreach ($params as $key => $value) {
                    if (in_array($key, $mustHave)) {
                        $args[$key] = $value;
                    }
                }
            }
        }

        return $args;
    }

    protected function checkUser($requiresAuth = false)
    {
        if (\Yii::$app->user->isGuest && $requiresAuth) {
            throw new \yii\web\ForbiddenHttpException(sprintf("You don't have access to ", $action));
        }
    }
}
