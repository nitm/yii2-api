<?php

namespace nitm\api\controllers;

use Yii;
use wukdo\rest\models\Entity;
use yii\web\NotFoundHttpException;
use nitm\helpers\Response;
use nitm\helpers\ArrayHelper;

/**
 * Base action for actions relating to the user and site content. i.e: Like, Favorite, Shares...etc
 */
class BaseActionController extends BaseActiveApiController
{
    public function beforeAction($action)
    {
        return parent::beforeAction($action);
    }

    /**
     * Check access based on action parameters which are boung by $params
     * @array $params
     * @return
     */
    public function checkAccess($action, $model=null, $params=[])
    {
        if (!\Yii::$app->user->identity) {
            throw new \yii\web\NotAuthorizedHttpException;
        }
        
        $class = ArrayHelper::remove($params, 'class', null);

        if ($class === null) {
            return null;
        }
        try {
            $model = $this->findModel($class::className(), null, [], [
                'where' => $params
            ]);
        } catch (NotFoundHttpException $e) {
            $model = null;
        };

        parent::checkAccess($this->action->id, $model, $params);
        return $model;
    }
}
