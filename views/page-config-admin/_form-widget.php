<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\tabs\TabsX;
use backend\widgets\Select2;
use nitm\helpers\Icon;
use nitm\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\AppConfig */
/* @var $form yii\widgets\ActiveForm */

$widgetClass = ArrayHelper::getValue($options, 'class', $class);
$options = @json_decode($options, true);
?>
<?php 
    if($widgetClass && class_exists($widgetClass)) {
        echo $widgetClass::widget(array_merge($options, [
            'class' => $widgetClass,
            'name' => $model->formName()."[config][$key][value]",
            'model' => $model,
            'value' => $value,
            'options' => [
                'id' => 'widget-config-'.uniqid(),
                'value' => $value
            ]
        ]));
?>
<?php
    } else {
        echo "Class ($class) doesn't exist!<br>";
    }
?>
    <a role="visibility" data-target="<?= $key ?>-extra-options" href="#">Show/Hide Options</a>
    <div id="<?= $key ?>-extra-options" style="display:none">
<?php
        echo Html::activeTextInput($model, "config[$key][class]", [
            'class' => 'form-control',
            'placeholder' => 'Enter Widget Class',
            'value' => $class
        ]);
        echo Html::activeTextarea($model, "config[$key][options]", [
            'class' => 'form-control',
            'placeholder' => 'Optional Widget Config, as json object',
            'value' => json_encode($options, JSON_PRETTY_PRINT)
        ]);
?>
    </div>
