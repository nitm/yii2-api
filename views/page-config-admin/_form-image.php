<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use kartik\widgets\ActiveForm;
use kartik\tabs\TabsX;
use backend\widgets\Select2;
use nitm\helpers\Icon;
use nitm\filemanager\models\Image;

/* @var $this yii\web\View */
/* @var $model common\models\AppConfig */
/* @var $form yii\widgets\ActiveForm */

$htmlId = Inflector::slug($key);
$htmlImageId = 'image-'.Inflector::slug($key);
?>
<?= Html::tag('div', \nitm\filemanager\widgets\ImageUpload::widget([
  "mode" => "single",
  "model" => new Image([
    'remote_type' => $model->is,
    'remote_id' => $model->id
  ]),
  'widgetOptions' => [
      'clientEvents' => [
        'fileuploaddone' => 'function(e, data) {
          updateFileValues(data.result.files[0].url)
        }',
      ]
  ]
]), [
  'style' => 'display:block'
]) ?>
<br>
<input id="<?= $htmlId ?>" type="hidden" name="PageConfig[config][<?=$key?>][value]"/>
<div class="thumbnail thumbnail-large" id="<?=$htmlImageId?>-wrapper" style="display: <?= $value ? 'block' : 'none';?>">
  <a href="#" class="text-danger text-center full-width" onclick="clearImage(event)">delete image</a>
  <img id="<?= $htmlImageId ?>" src="<?= @$value ?>">
  <a href="#" class="text-danger text-center full-width" onclick="clearImage(event)">delete image</a>
</div>

<script>
document.addEventListener("DOMContentLoaded", function() {
  updateFileValues('<?=$value?>')
});
function updateFileValues(value) {
  if(value) {
    $("#<?=$htmlId?>").val(value);
    $("#<?=$htmlImageId?>").attr("src", value);
    $("#<?=$htmlImageId?>-wrapper").slideDown()
  } else {
    $("#<?=$htmlImageId?>-wrapper").slideUp()
  }
}

function clearImage(event) {
  event.preventDefault();
  $('#<?= $htmlId ?>').val(null);
  $('#<?= $htmlImageId ?>').attr('src', null);
  $("#<?=$htmlImageId?>-wrapper").slideUp()
}
</script>