<?php

use yii\helpers\Html;
use nitm\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Page Configs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-config-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a(Yii::t('app', 'Create Page Config'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'page' => [
                'attribute' => 'page',
                'format' => 'html',
                'value' => function($model) {
                    return Html::a($model->pageName, \Yii::$app->urlManager->createUrl(['page-config-admin/update', 'id' => $model->id]));
                }
            ],
            'class_name',
            'created_at:datetime',
            // 'deleted_at',

            ['class' => 'nitm\grid\ActionColumn'],
        ],
    ]); ?>
</div>
