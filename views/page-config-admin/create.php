<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model nitm\api\models\PageConfig */

$this->title = Yii::t('app', 'Create Page Config');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Page Configs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-config-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
