<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$listId = isset($listId) ? $listId : 'page-config-list';
?>
<div class="media-list" id="<?=$listId?>">
<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'options' => [
        'class' => 'media-list',
        'id' => $listId
    ],
    'summary' => false,
    'emptyText' => '',
    'itemView' => function ($configs, $key) use ($model) {
        foreach($configs as $config) {
            echo $this->render('_form-input', [
                'id' => $model->htmlId($config['key']),
                'model' => $model,
                'key' => $config['key'],
                'type' => $config['type'],
                'value' => ArrayHelper::getValue($config, 'value'),
                'class' => ArrayHelper::getValue($config, 'class'),
                'options' => ArrayHelper::getValue($config, 'options', []),
                'section' => $config['section'],
                'title' => $config['key'],
                'listId' => $model->htmlParentId($config['key']),
                'children' => $this->render('list', [
                    'model' => $model,
                    'listId' => $model->htmlId($config['key']).'-children',
                    'dataProvider' => new \yii\data\ArrayDataProvider([
                        'allModels' => ArrayHelper::getValue($config, 'children', [])
                    ])
                ])
            ]);
        }
    },
]); ?>
</div>
