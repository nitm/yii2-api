<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\tabs\TabsX;
use backend\widgets\Select2;
use nitm\helpers\Icon;

/* @var $this yii\web\View */
/* @var $model common\models\AppConfig */
/* @var $form yii\widgets\ActiveForm */
?>
<?= Html::activeRadioList($model, "config[$key][value]", [
    0 => 'False',
    1 => 'True',
    'options' => [
        'value' => $value
    ]
]) ?>
