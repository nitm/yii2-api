<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use backend\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\AppConfig */
/* @var $form yii\widgets\ActiveForm */

$defaultFormOptions = [
    'type' => 'vertical'
];
$formOptions = isset($formOptions) ? array_merge($defaultFormOptions, $formOptions) : $defaultFormOptions;
$formOptions['options']['role'] = 'ajaxForm';
$isAjax = true;
?>

<div class="app-config-form">
    <p class="alert alert-info">Create a new config by entering a name and selecting the type. You can group config items by using a period <strong>(.)</strong>. For example for all image configs you can use <strong>image.main</strong> and <strong>image.loading</strong></p>
    <?php
        $form = include(\Yii::getAlias('@nitm/views/layouts/form/header.php'));
    ?>
    <?= $form->field($model, 'page')->textInput([
            'placeholder' => '[Required] Page'
        ]) ?>
    <?= $form->field($model, 'configKey', [
        'addon' => [
            'prepend' => [
                'content' => Html::activeDropdownList($model, 'configType', [
                    'bool' => 'Boolean Value',
                    'number' => 'Number Value',
                    'image' => 'Image',
                    'images' => 'Images',
                    'paragraph' => 'Paragraph',
                    'string' => 'String',
                    'widget' => 'Widget'
                ], [
                    'onchange' => 'checkIfNeedsExtraFields(event)',
                    'prompt' => [
                        'options' => [
                            'class' => 'prompt'
                        ],
                        'text' => "Select config type"
                    ]
                ])
            ],
            'append' => [
                'content' => Html::button('Add', [
                    'class' => 'btn btn-primary',
                    'role' => 'appender',
                    'data-url' => 'get-field?id='.$model->getId().'&do=true',
                    'data-before-send' => 'prepareGetFieldData',
                    'data-parent' => '#page-config-list'
                ]),
                'asButton' => true
            ]
        ]
    ])->textInput([
        'placeholder' => 'Group using dot notation. .ie.: image.main, image.loading'
    ])->label('Add Config Options', [
        'class' => 'col-lg-12 col-sm-12'
    ]) ?>
    <div id="extra-options" style="display:none">
        <?= $form->field($model, 'configClass'); ?>
        <?= $form->field($model, 'configOptions')->textarea(); ?>
    </div>
    <?= $this->render('list', [
        'model' => $model,
        'dataProvider' => new \yii\data\ArrayDataProvider([
            'allModels' => $model->toTree()
        ])
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
function checkIfNeedsExtraFields(event) {
    if($(event.target).val() === 'widget') {
        $('#extra-options').slideDown();
    } else {
        $('#extra-options').slideUp();
    }
}
function prepareGetFieldData() {
    let configType = $("#pageconfig-configtype").val(),
        configKey = $("#pageconfig-configkey").val()
        configClass = null, configOptions = null;
    if(!configKey || !configType) {
        return "Empty config type and key. Please make sure they're set";
    }
    if(configType == 'widget') {
        configClass = $("#pageconfig-configclass").val(),
        configOptions = $("#pageconfig-configoptions").val();
        if(!configClass) {
            return "You selected a widget option but didn't provide a class";
        }
    }
    $(this).data("data", {
        configType: configType,
        configKey: configKey,
        configClass: configClass,
        configOptions: configOptions
    });
    return true;
}
</script>
