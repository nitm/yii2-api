<?php

use yii\helpers\Html;
use yii\helpers\Inflector;
use kartik\widgets\ActiveForm;
use kartik\tabs\TabsX;
use backend\widgets\Select2;
use nitm\helpers\Icon;

/* @var $this yii\web\View */
/* @var $model common\models\AppConfig */
/* @var $form yii\widgets\ActiveForm */

$defaultFormOptions = [
    'type' => 'inline'
];
$formOptions = isset($formOptions) ? array_merge($defaultFormOptions, $formOptions) : $defaultFormOptions;
$isAjax = true;
$keyString = substr($key, 0, strrpos($key, '.') ?: strlen($key));
$titleString = substr($title, strpos($title, '.') ? strpos($title, '.')+1 : 0);
?>
<div class="page-config-form-<?=$type?>row" id="<?=$id?>">
    <?= Html::tag('h3', Inflector::humanize($keyString)); ?>
    <div style="display:flex;">
        <div class="col-md-3 col-lg-2 col-sm-4" style="display:flex; align-items: center; justify-content: flex-end">
                <strong><?= $titleString ?>&nbsp;</strong>
                <?= Html::a(Icon::show('trash'), '#', [
                    'data-confirm' => "Are you sure you want to delete the ".$key." value?",
                    "title" => "Delete ".$key,
                    'role' => 'removeParent',
                    'data-parent' => "#$id",
                    'class' => 'text-danger'
                ]); ?>
        </div>
        <div class="col-md-9 col-lg-10 col-sm-8">
            <div style="padding: 10px 10px 0px 0px">
                <?= Html::activeHiddenInput($model, "config[$key][key]", [
                    'value' => $key
                ]); ?>
                <?= Html::activeHiddenInput($model, "config[$key][type]", [
                    'value' => $type
                ]); ?>
                <?= Html::activeHiddenInput($model, "config[$key][section]", [
                    'value' => $section
                ]); ?>
                <?= $this->render('_form-'.$type, [
                    'key' => $key,
                    'model' => $model,
                    'class' => $class,
                    'options' => $options,
                    'value' => isset($value) ? $value : null
                ])?>
            </div>
            <?= @$children ?>
        </div>
    </div>
</div>
