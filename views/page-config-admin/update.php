<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model nitm\api\models\PageConfig */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Page Config',
]) . $model->title();
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Page Configs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="page-config-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
