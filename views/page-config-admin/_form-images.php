<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use kartik\widgets\ActiveForm;
use kartik\tabs\TabsX;
use backend\widgets\Select2;
use nitm\helpers\Icon;

/* @var $this yii\web\View */
/* @var $model common\models\AppConfig */
/* @var $form yii\widgets\ActiveForm */

$htmlId = Inflector::slug($key);
$htmlImageId = 'image-'.Inflector::slug($key);
?>
<?= Html::tag('div', \nitm\filemanager\widgets\Images::widget([
  "model" => new Image([
    'remote_type' => $model->is,
    'remote_id' => $model->id
  ]),
  'widgetOptions' => [
      'clientEvents' => [
        'fileuploaddone' => 'function(e, data) {
          $(e.currentTarget).addClass("disabled btn-default loading");
          let inputName = $(e.currentTarget).data("name"),
            $form = (e.currentTarget.form),
            $contianer = $("#page-config-'.$htmlId.'");
          $container.find(\'[role="imagesContainer"]\').append(data.result.data);
          $form.append("<input type=\'hidden\' name=\'"+PageConfig[config]['.$key.'][value][]+"\' value=\'"+data.result.files[0].url+"\'/>");
        }',
      ]
  ],
  'afterImage' => function ($image) use($model, $key) {
    return Html::activeHiddenInput($model, "config[$key][value][]", [
      'value' => $image->url()
    ]);
  }
]), [
  'style' => 'display:block'
]) ?>
