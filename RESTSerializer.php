<?php
/**
 * @link http://www.nitm\api.com/
 *
 * @copyright Copyright (c) 2015 PickledUp Inc
 */

namespace nitm\api;

use yii\helpers\ArrayHelper;
use yii\web\Link;

/**
 * Custom serializer based on \yii\rest\Serializer.
 *
 * @author Malcolm Paul <lefteyecc@wukdo.com>
 */
class RESTSerializer extends Serializer
{
    protected function organize($properties, $type, $modelFields)
    {
        list($fields, $extraFields, $allFields) = $modelFields;
        $ret_val = [];
        $properties = $this->normalize($type, $properties);
        foreach ($properties as $k => $v) {
            if (is_null($v)) {
                continue;
            }
            //We have a global relation that other models may also have

            if (($options = static::normalizerFor($k)) !== false) {
                $v = call_user_func_array([$this, 'normalizeSpecial'], [$v, $options]);
            }

            if (in_array($k, $extraFields) && in_array($k, $fields) && is_array($v)) {
                $ret_val[$k] = $this->normalize($k, $v);
                if (is_array($v) && ArrayHelper::isIndexed($v)) {
                    $ret_val[$type][$k] = ArrayHelper::getColumn($v, 'id');
                } else {
                    $ret_val[$type][$k] = $v['id'];
                }
            } elseif (in_array($k, $extraFields) && !in_array($k, $fields) && is_array($v)) {
                if (is_array($v) && ArrayHelper::isIndexed($v)) {
                    $ret_val[$k] = ArrayHelper::getColumn($v, 'id');
                } else {
                    $ret_val[$k] = $v['id'];
                }
            } else {
                $ret_val[$type][$k] = $v;
            }
        }

        return $ret_val;
    }
}
