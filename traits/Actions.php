<?php

namespace nitm\api\traits;

use yii\base\Model;

/**
 * Trait ApiContent.
 */
trait Actions
{
    /**
    * Is this action cachable?
    *
    * @var bool
    */
   public $cachable = true;

    public function findModel($id, $options = [], $extraFields = true)
    {
        return $this->findModelInternal($id, $options, $extraFields);
    }

    /**
     * Returns the data model based on the primary key given.
     * If the data model is not found, a 404 HTTP exception will be raised.
     *
     * @param string $id the ID of the model to be loaded. If the model has a composite primary key,
     *                   the ID must be a string of the primary key values separated by commas.
     *                   The order of the primary key values should follow that returned by the `primaryKey()` method
     *                   of the model
     *
     * @return ActiveRecordInterface the model found
     *
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelInternal($id, $options = [], $extraFields = true)
    {
        if ($this->findModel !== null) {
            return call_user_func_array($this->findModel, [$id, $options, $extraFields], $this);
        }

        /* @var $modelClass ActiveRecordInterface */
        $modelClass = $this->modelClass;
        $searchModel = new $modelClass($options);
        $searchModel->beginSearch($options);
        $dataProvider = $searchModel->search(array_merge([
            $searchModel->formName() => $this->getParams($modelClass, $id),
        ], $options));
        $model = $dataProvider->query->one();

        $request = \Yii::$app->request;
        if ($extraFields && !$request->get('expand')) {
            $_GET['expand'] = implode(',', array_keys($searchModel->extraFields()));
        }

        if ($model) {
            return $model;
        } else {
            throw new \yii\web\NotFoundHttpException($searchModel->isWhat()." not found: $id");
        }
    }

    public function getParams($modelClass, $id)
    {
        return [current($modelClass::primaryKey()) => $id];
    }

    public function resolveData($key=null, $default=null) {
        return \nitm\helpers\RequestHelper::resolveData($key, $default);
    }
}
