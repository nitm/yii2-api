<?php

namespace nitm\api\traits;

use nitm\helpers\Response;
use dektrium\user\models\LoginForm;
use dektrium\user\models\User;
use nitm\api\helpers\Auth as AuthHelper;

trait UserController
{
    protected function defaultBehaviors()
    {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => \Yii::$app->getModule('rest-api')->corsHeaders,
            ],
        ];
    }

    protected function updateLastActive()
    {
        $class = $this->apiModule->getModelClass('LoginForm');
        $class::updateLastActive();
    }

    public function loginByToken()
    {
        $token = $this->accessToken;
        if ($token) {
            if (\Yii::$app->user->loginByAccessToken($token)) {
                return \Yii::$app->user->identity;
            }

            return false;
        }

        return false;
    }

    public function beforeAction($action)
    {
        AuthHelper::setReturnUrl();
        $this->enableCsrfValidation = false;
        parent::beforeAction($action);
         /**
          * For some retarded reason yii2 Cors is not setting certain headers. Setting them manually.
          */
         \yii\filters\Cors::addCorsHeaders(\Yii::$app->getResponse(), \Yii::$app->getModule('rest-api')->corsHeaders);
        //Some support for OPTIONS requests
        switch (\Yii::$app->request->method) {
            case 'OPTIONS':
            case 'HEAD':
            return false;
            break;
        }
        if (!$this->isResponseFormatSpecified) {
            $this->responseFormat = 'json';
        }

        //Set the return URl. Specifically for re-auth actions
        \nitm\api\helpers\Auth::setReturnUrl();

        return true;
    }

    protected function getUserInfo($id = null, $with = false)
    {
        $class = \Yii::$app->getUser()->identityClass;
        $key = 'user';
        $with = $with ?: ['profile'];
        if($id == null) {
          $user = \Yii::$app->getUser()->getIdentity();
        } else {
          $query = $class::find();
          if(is_numeric($id)) {
            $query->where(['id' => $id]);
          } else {
            $query->orWhere([
              'email' => $id
            ])->orWhere([
              'username' => $id,
            ]);
          }
          $user = $query->one();
        }

        return [$key => $user];
    }

    /**
     * [appendAccessToken description]
     * @param  [type] $user [description]
     * @return [type]       [description]
     */
    protected function appendAccessToken($user = null)
    {
        //Add token to the headers
        $headers = \Yii::$app->response->headers;
        $user = $user ?? \Yii::$app->user->identity;
        if (!$user->apiToken) {
            $user->generateApiToken();
        }
        $headers->set('Access-Token', $user->apiToken->token);
    }
}
