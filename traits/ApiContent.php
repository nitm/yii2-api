<?php

namespace nitm\api\traits;

use Yii;
use yii\base\Model;
use yii\base\Event;
use nitm\helpers\ArrayHelper;

/**
 * Trait ApiContent
 * @package wukdo
 */

trait ApiContent
{
    protected function getRelationIdString($model)
    {
        try {
            $type = ArrayHelper::getValue(\Yii::$app, 'controller.modelType', null);
        } catch (\Exception $e) {
            $type = null;
        }
        if (!$type) {
            $type = $model->isWhat();
        }
        return $type .= '_id';
    }

    protected function getUserActionArray($model, $type)
    {
        $actionCount = $type.'Count';
        $isActioned = 'is'.ucfirst($type).'d';
        return [
            'id' => $model->isWhat(false).$model->getId(),
            'remoteType' => $model->isWhat(),
            'remoteId'  => $model->id,
            'count' => (int) $model->$actionCount(),
            $isActioned => (boolean) ArrayHelper::getValue($model->$type(), 'user_has_action', false),
            $this->getRelationIdString($model) => $model->id
        ];
    }

    /**
     * Get a value from metadata
     * @param  array $from      The array to search in
     * @param  string $attribute The attribute to return
     * @param  string $type      The type of value to return from the metadata
     * @return mixed            The metadata value
     */
    protected function getImageDims(array $from, $attribute='height', $type='min-height')
    {
        $ret_val = null;
        foreach ((array)$from as $data) {
            $value = ArrayHelper::getValue($data, $attribute, 0);
            switch ($type) {
                case 'min-height':
                case 'min-width':
                if (is_null($ret_val)) {
                    $ret_val = $value;
                } else {
                    $ret_val = ($value <= (int)$ret_val) ? $value : (int)$ret_val;
                }
                break;

                case 'max-height':
                case 'max-width':
                $ret_val = ($value >= (int)$ret_val) ? $value : (int)$ret_val;
                break;

                default:
                $ret_val = $value;
                break;
            }
        }
        return ($ret_val >= 100 ? $ret_val : 100);
    }
}
