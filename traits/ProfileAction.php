<?php

namespace nitm\api\traits;

use Yii;
use yii\base\Model;
use yii\base\Event;
use nitm\helpers\ArrayHelper;
use nitm\api\models\User;

/**
 * Trait ApiContent
 * @package wukdo
 */

trait ProfileAction
{
    public function getParams($modelClass, $id)
    {
        if (!is_numeric($id)) {
            $where = static::getParamsForUserQuery($modelClass, $id);
            //If we alredy have the ID no need to search for it
            if (count($where) == 1 && is_numeric(current($where))) {
                $id = current($where);
            } else {
                $id = new \yii\db\Expression("(".(User::find()
                    ->select('id')
                    ->where($where)
                    ->limit(1)->createCommand()->getRawSql()).")");
            }
        }
        return [
            'user_id' => $id
        ];
    }
}
