<?php

namespace wukdo\rest\traits\relations;

use Yii;
use yii\base\Model;
use yii\base\Event;
use nitm\helpers\ArrayHelper;
use nitm\models\log\DbEntry;
use wukdo\rest\models\Friend;

/**
 * Trait User
 * @package wukdo
 */

trait User
{
	public function hasRetrievedFriendsFrom($network)
	{
		return ArrayHelper::keyExists('get-network-friends-'.$network, $this->retrievedNetworkFriends());
	}

	public function hasRetrievedAllNetworkFriends()
	{
		$ret_val = true;
		if($this->retrievedNetworkFriends() == [] && count($this->accounts) >= 1) {
			$ret_val = false;
		} else {
			foreach($this->retrievedNetworkFriends() as $network)
			{
				if($network && $network['_count'] == 0) {
					$ret_val = false;
					break;
				}
			}
		}
		return $ret_val;
	}

	public function retrievedNetworkFriends()
	{
		return $this->retrievedNetworkFriends;
	}

	public function getRetrievedNetworkFriends()
	{
		return $this->hasMany(DbEntry::className(), ['user_id' => 'id'])
			->select([
				'user_id',
				'_count' => 'COUNT(*)',
				'action'
			])
			->where(['like', 'action', 'get-network-friends'])
			->asArray()
			->indexBy('action')
			->groupBy(['user_id', 'action']);
	}

	public function getFriends()
	{
		return $this->hasMany(Friend::className(), [
				'user_id' => 'id'
			])
			->from(['f' => Friend::tableName()])
			->select([
                '*',
                'isMutual' => "(".new \yii\db\Expression(Friend::find()
					->from(["fc" => Friend::tableName()])
					->select("COUNT(*)")
                    ->where('fc.friend_id=f.user_id AND fc.user_id=f.friend_id')
					->limit(1)
					->createCommand()->getRawSql()).")"
			])
			->andWhere([
				'f.deleted' => false
			])
			->with('friend', 'user'); //Can't eager load friend because of quirk in yii?
	}
}
?>
