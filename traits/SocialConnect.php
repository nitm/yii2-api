<?php

namespace nitm\api\traits;

use Yii;
use yii\base\Model;
use yii\base\Event;
use nitm\helpers\ArrayHelper;
use nitm\helpers\RequestHelper;
use dektrium\user\models\User;
use nitm\api\helpers\Auth as AuthHelper;

/**
 * Trait ApiContent
 * @package wukdo
 */

trait SocialConnect
{
    /**
     * Social account connector helper
     * @param  [type] $code    [description]
     * @param  [type] $account [description]
     * @return [type]          [description]
     */
    protected function connectAccount($code=null, $account=null)
    {
        if (!$account) {
            if (\Yii::$app->request->isAjax || $this->forceAjax) {
                return [
               'error' => [
                   'message' => 'Whoops. This code is invalid. Try connecting your account again!',
               ],
           ];
            } else {
                $this->redirect(AuthHelper::getReturnUrl(['code' => 'error-connect']));
            }
        } elseif ($account && $account->getIsConnected()) {
            return $this->connectAndRespond($account);
        } else {

           /** @var User $user */
           $user = User::find()->where(['email' => $account->email])->one();
           if(!$user) {
             $user = Yii::createObject([
                 'class' => User::className(),
                 'scenario' => 'connect',
                 'username' => $account->username,
                 'email'    => $account->email ?? $account->username,
             ]);
           }

            $event = $this->getConnectEvent($account, $user);

            $this->trigger(self::EVENT_BEFORE_CONNECT, $event);

            $data = $this->resolveData($user->formName(), $this->resolveData());

            ArrayHelper::remove($data, 'code');
            //Create the user if they haven't been created
            if($user->isNewRecord) {
              $user->load($data, '') && $user->create();
            }
            //If the user exists then connect and respond to the request
            if(!$user->isNewRecord) {
                $account->connect($user);

                return $this->connectAndRespond($account);
            } else {
                if (\Yii::$app->request->isAjax || $this->forceAjax) {
                    //there Was an atempt to create an account
                 $errors = [
                     'error' => array_merge(['message' => 'There was some trouble processing your info'], $user->getErrors()),
                 ];

                    return $errors;
                } else {
                    $this->redirect(AuthHelper::getReturnUrl([
                    'code' => $code,
                 ]));
                }
            }
        }
    }

    /**
     * After an account is connected, login and return the appropriate response.
     *
     * @method connectAndRespond
     *
     * @param [type] $account [description]
     *
     * @return [type] [description]
     */
    protected function connectAndRespond($account)
    {
        Yii::$app->user->login($account->user, \Yii::$app->getModule('user')->rememberFor);
        $event = $this->getConnectEvent($account, $account->user);
        $this->trigger(self::EVENT_AFTER_CONNECT, $event);
        if (\Yii::$app->request->isAjax || $this->forceAjax) {
            return array_merge($this->getUserInfo(), [
             'connect' => [
                 'success' => true,
             ],
         ]);
        } else {
            $this->redirect(AuthHelper::getReturnUrl([
            'code' => $account->code,
         ]));
        }
    }
}
