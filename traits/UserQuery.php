<?php

namespace nitm\api\traits;

use Yii;
use yii\base\Model;
use yii\base\Event;
use nitm\helpers\ArrayHelper;

/**
 * Trait ApiContent
 * @package wukdo
 */

trait UserQuery
{
    /**
     * Expects to recieve a model class and an id to get the DB query to find a user
     * @param  string $modelClass		The class being searched
     * @param  string|int $id         	The username or id of the user
     * @return array             		The query WHERE|Filter parameters
     */
    public function getParamsForUserQuery($modelClass, $id)
    {
        $searchType = 'id';
        $keys = ['id'];
        if ($id == 'me' && !\Yii::$app->getUser()->isGuest) {
            $id = \Yii::$app->getUser()->getIdentity()->getId();
        } else {
            if (strpos($id, '@') !== false) {
                $keys = ['email'];
            } else {
                $keys = ['username'];
            }
            $searchType = 'string';
        }
        return array_combine($keys, array_fill(0, count($keys), $id));
    }
}
