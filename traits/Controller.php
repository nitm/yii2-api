<?php

namespace nitm\api\traits;

use nitm\helpers\ArrayHelper;
use nitm\helpers\Json;
use nitm\api\models\User;

trait Controller
{
    public $tokenParam = 'access_token';
    public $forceAjax = false;
    protected $_apiModule;

    public function getApiModule()
    {
        return \Yii::$app->getModule('rest-api');
    }

    public function getAccessToken()
    {
        $request = \Yii::$app->request;
        $token = $request->get($this->tokenParam);
        if (!$token) {
            $authHeader = \Yii::$app->request->getHeaders()->get('Authorization');
            preg_match('/^Bearer\s+(.*?)$/', $authHeader, $matches);
            if ($authHeader) {
                $token = $matches[1];
            }
        }
        return $token;
    }

    public function resolveData($key=null, $default=null)
    {
        return \nitm\helpers\RequestHelper::resolveData($key, $default);
    }
}
